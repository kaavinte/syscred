
<section class="wrapper">

    <div class="col-lg-6">


        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-adjust"></span> TOTAL DE CRÉDITOS</strong></div>
            <div class="panel-body">

                <section class="panel tasks-widget">

                    <div class="panel-body">

                        <section class="panel">
                           
                            <div class="panel-body text-center">
                                <canvas id="doughnut" height="200" width="200"style="width: auto;  height: auto;"></canvas>
                            </div>
                            
<!--                            <div class="row">
                                <div class="col-lg-4">
                                    <div style="width: 28px; height: 30px; background-color:#F7464A;">
                                </div>
                                 <div class="col-lg-4">
                                    TOTAL DE CRÉDITOS PARA ATINGIR
                                </div>
                            </div>-->
                            
<!--                            <div>LEGENDA:</div>
-->                            <div style="width: 28px; height: 30px; background-color:#F7464A;margin-top: 5px; float: left;"><span style="margin-left: 6px; color: white; font-weight: bold; margin-top: 10px; font-size: 15px;"><?php echo  $totalcredito['credito'] - $totalAtual['totalatual']  ?></span></div> <div style="margin-left: 36px; margin-top: -25px; width: 100%; float: left;"><b>CRÉDITOS QUE FALTAM</b></div><!--
-->                            <div style="width: 28px; height: 30px; background-color:#46BFBD;;margin-top: 5px; float: left;"><span style="margin-left: 6px; color: white; font-weight: bold; margin-top: 10px; font-size: 15px;"><?php echo $totalAtual['totalatual']; ?></span></div> <div style="margin-left: 36px; margin-top: -25px; width: 100%; float: left;"><b>SEUS CRÉDITOS</b></div><!--
                            <div style="width: 28px; height: 30px; background-color:#46BFBD; margin-top: 10px; "></div>
                            -->
                            
                        </section>

                    </div>
                </section>

                <hr/>

                <?php echo form_close(); ?>

            </div>
        </section>
    </div>
    
    
    <div class="col-lg-6">


        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-adjust"></span> TOTAL HORAS ATIVIDADES</strong></div>
            <div class="panel-body">

                <section class="panel tasks-widget">

                    <div class="panel-body">

                        <section class="panel">
                           
                            <div class="panel-body text-center">
                                <canvas id="doughnut2" height="200" width="200"style="width: auto;  height: auto;"></canvas>
                            </div>
                            
<!--                            <div class="row">
                                <div class="col-lg-4">
                                    <div style="width: 28px; height: 30px; background-color:#F7464A;">
                                </div>
                                 <div class="col-lg-4">
                                    TOTAL DE CRÉDITOS PARA ATINGIR
                                </div>
                            </div>-->
                            
<!--                            <div>LEGENDA:</div>
-->                            <div style="width: 28px; height: 30px; background-color:#FDB45C;margin-top: 5px; float: left;"><span style="margin-left: 1px; color: white; font-weight: bold; margin-top: 10px; font-size: 15px;"><?php echo $cursos['ativ_comp_obrigatoria']-$totalAtividades['cargaTotal']; ?></span></div> <div style="margin-left: 36px; margin-top: -25px; width: 100%; float: left;"><b>HORAS ATIV. QUE FALTAM</b></div><!--
-->                            <div style="width: 28px; height: 30px; background-color:#4D5360;;margin-top: 5px; float: left;"><span style="margin-left: 6px; color: white; font-weight: bold; margin-top: 10px; font-size: 15px;"><?php echo $totalAtividades['cargaTotal']; ?></span></div> <div style="margin-left: 36px; margin-top: -25px; width: 100%; float: left;"><b>SUAS HORAS COMPLEMENTARES</b></div><!--
                            <div style="width: 28px; height: 30px; background-color:#46BFBD; margin-top: 10px; "></div>
                            -->
                            
                        </section>

                    </div>
                </section>

                <hr/>

                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<script src="<?php echo base_url(); ?>template/assets/chart-master/Chart.js"></script>
<script src="<?php echo base_url(); ?>template/js/all-chartjs.js"></script>
<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<!-- script for this page only-->



<script>
    var Script = function () {
        var doughnutData = [
            {
                value: <?php echo  $totalcredito['credito']-$totalAtual['totalatual'];?>,
                color: "#F7464A"
            },
            {
                value: <?php echo $totalAtual['totalatual']; ?>,
                color: "#46BFBD"
            }

        ];
        
        
        


        new Chart(document.getElementById("doughnut").getContext("2d")).Doughnut(doughnutData);
    }();
</script>


<script>
    var Script = function () {
        var doughnutData2 = [
            {
                value: <?php echo $cursos['ativ_comp_obrigatoria']-$totalAtividades['cargaTotal']; ?>,
                color: "#FDB45C"
            },
            {
                value: <?php echo $totalAtividades['cargaTotal']; ?>,
                color: "#4D5360"
            }

        ];
        
        
        new Chart(document.getElementById("doughnut2").getContext("2d")).Doughnut(doughnutData2);
    }();
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>template/js/bootstrap-inputmask.min.js"></script>