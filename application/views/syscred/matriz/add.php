<section class="wrapper">

    <div class="col-lg-6">

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> CADASTRO DE MATRIZ</strong></div>
            <div class="panel-body">

                <?php echo form_open('matriz/add', array('enctype' => 'multipart/form-data')); ?>

                <div class="row">

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Nome </label>
                            <input type="text" required="required" name="nome"  class="form-control"  >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Ano</label>
                            <input type="text" name="ano"  required="required" class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Curso</label>

                            <select name="curso_id" required="required" class="form-control">
                                <option value="">Selecione o Curso</option>

                                <?php
                                foreach ($cursos as $row):
                                    ?>
                                    <option value="<?php echo $row['curso_id'] ?>"><?php echo $row['descricao']; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>

                        </div>
                    </div>
                </div>
                <hr/>


                <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>template/js/bootstrap-inputmask.min.js"></script>