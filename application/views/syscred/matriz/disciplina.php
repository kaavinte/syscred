<section class="wrapper">

    <div class="col-lg-12">

        <?php if ($this->session->flashdata('message') != ""): ?>

            <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <?php echo $this->session->flashdata('message'); ?>

            </div>
        <?php endif; ?>

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> ADICIONANDO DISCIPLINA</strong></div>
            <div class="panel-body">

                <section class="panel tasks-widget">
                    <header class="panel-heading">


                        <?php
                        echo $matriz['nome'];
                        ?>
                        <hr/>


                        <?php echo form_open('matriz/DiscipinaAdd/' . $matriz['matriz_id'], array('enctype' => 'multipart/form-data')); ?>

                        <div class="row" style="font-size: 13px;">

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Disciplina</label>
                                    <input type="text" required="required"  name="disciplina"  class="form-control"  >
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Série</label>
                                    <input type="text" name="serie"  class="form-control" >
                                </div>
                            </div>


                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>Credito</label>
                                    <input type="text" name="credito"  class="form-control" >
                                </div>
                            </div>


                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>CH</label>
                                    <input type="text" name="ch"  class="form-control" >
                                </div>
                            </div>


                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>CHP</label>
                                    <input type="text" name="chp"  class="form-control" >
                                </div>
                            </div>

                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label>CHO</label>
                                    <input type="text" name="cho"  class="form-control" >
                                </div>
                            </div>



                            <div class="col-lg-2">
                                <div class="form-group">
                                    <input style="margin-top: 25px;" type="submit" class="btn btn-primary" value="SALVAR"></input>
                                </div>
                            </div>

                        </div>

                        <?php echo form_close(); ?>



                    </header>
                    <div class="panel-body">

                        <table style="font-size: 12px;" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nº</th>
                                    <th>Código</th>
                                    <th>Disciplina</th>
                                    <th>Série</th>
                                    <th>Crédito</th>
                                    <th>C.H. Teor</th>
                                    <th>C.H. Prát</th>
                                    <th>C.H. Outros</th>
                                    <th>C.H. Total</th>
                                    <th>AÇÕES</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cont = 1;
                                foreach ($disciplinas as $row):
                                    ?>
                                    <tr>
                                        <td><?php echo $cont++; ?></td>
                                        <td><?php echo $row['matriz_disciplina_id']; ?></td>
                                        <td><?php echo $row['disciplina']; ?></td>

                                        <td><?php echo $row['serie']; ?></td>
                                        <td><?php echo $row['credito']; ?></td>
                                        <td><?php echo $row['ch']; ?></td>
                                        <td><?php echo $row['chp']; ?></td>
                                        <td><?php echo $row['cho']; ?></td>
                                        <td><?php echo $row['chtotal']; ?></td>
                                        <td><a href="<?php echo base_url(); ?>matriz/deleteDisciplina/<?php echo $row['matriz_disciplina_id']; ?>/<?php echo $matriz['matriz_id']; ?>"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            </tbody>


                            <thead>
                                <tr>
                                    <th>TOTAL DE HORAS E CRÉDITOS</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?php echo $SumCredito['credito']; ?></th>
                                    <th><?php echo $SumCht['cht']; ?></th>
                                    <th><?php echo $Sump['chp']; ?></th>
                                    <th><?php echo $Sumo['cho']; ?></th>
                                    <th><?php echo $SumTotal['total']; ?></th>
                                    <th></th>
                                </tr>
                            </thead>


                        </table>

                    </div>
                </section>

                <hr/>

                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>template/js/bootstrap-inputmask.min.js"></script>