<section class="wrapper site-min-height">

    <h1 style="font-weight: 300;"><span class="fa   fa-pencil-square"></span> LISTA DE MATRIZ</h1>
    <hr style="border: 1px solid #333;">
    <div class="divider"></div>
    <div class="divider"></div>



    <div class="row">
        <div class="col-lg-12">

            <?php if ($this->session->flashdata('message') != ""): ?>

                <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <?php echo $this->session->flashdata('message'); ?>

                </div>
            <?php endif; ?>

            <section class="panel">

                <header class="panel-heading">
                    <a href="<?php echo base_url(); ?>matriz/add"><button class="btn btn-primary"><span class="glyphicon glyphicon-plus">
                            </span> MATRIZ</button>
                    </a>
                </header>

                
                <div class="panel-body">
                    <div class="adv-table" style="overflow-x: auto">

                        <table  class="display table table-bordered table-striped" id="example" style="font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>NOME</th>
                                    <th>ANO</th>
                                    <th>CURSO</th>
                                    <th>AÇÕES</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cont = 1;
                                foreach ($matriz as $row):
                                    ?>
                                    <tr>
                                        <td style="width: 3%" ><?php echo $cont++; ?></td>
                                        <td style="width: 25%"><?php echo $row['nome']; ?></td>
                                        <td>
                                            <?php echo $row['ano']; ?>
                                        </td>

                                        <td><?php echo $row['descricao']; ?></td>
                                        <td style=" width: 25%; text-align: center">
                                            <a href="<?php echo base_url(); ?>matriz/disciplinas/<?php echo $row['matriz_id']; ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-bars"></i> Disciplinas</button></a>
                                            <a href="<?php echo base_url(); ?>matriz/edit/<?php echo $row['matriz_id']; ?>"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-refresh"></i> Editar</button></a>
                                            <a href="<?php echo base_url(); ?>matriz/delete/<?php echo $row['matriz_id']; ?>"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Excluir</button></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>



                            </tbody>

                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/assets/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').dataTable({
            "aaSorting": [[6, "desc"]]
        });
    });
</script>