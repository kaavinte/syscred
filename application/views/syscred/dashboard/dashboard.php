<?php
if ($this->session->userdata('type_login') == "admin") {
    ?>
    <section class="wrapper">
        <!--state overview start-->
        <div class="row state-overview">
            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                    <div class="symbol terques">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="value">
                        <h1 class="count">
                            <?php echo $CountAluno ?>
                        </h1>
                        <p>TOTAL DE ALUNOS</p>
                    </div>
                </section>
            </div>

        </div>
        <!--state overview end-->


    </section>

    <?php
} else {
    ?>

    <section class="wrapper">
        <!--state overview start-->
        <div class="row state-overview">
            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                    <div class="symbol terques">
                        <i class="fa fa-credit-card"></i>
                    </div>
                    <div class="value">
                        <h1 class="count">
                            <?php echo $totalAtual['totalatual'] ?>
                        </h1>
                        <p>TOTAL DE CRÉDITOS</p>
                    </div>
                </section>
            </div>
            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                    <div class="symbol red">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="value">
                        <h1 class=" count2">
                            <?php echo $totalHora['chtotal'] ?>
                        </h1>
                        <p>TOTAL CARGA HORÁRIA</p>
                    </div>
                </section>
            </div>



            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                    <div class="symbol blue">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="value">
                        <h1 class=" count3">
                            <?php echo $totalAtividades['cargaTotal'] ?>
                        </h1>
                        <p>TOTAL HORA ATIVIDADES</p>
                    </div>
                </section>
            </div>
            
            
            
              <div class="col-lg-3 col-sm-6">
                <section class="panel">
                    <div class="symbol yellow">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="value">
                        <h1 class=" count3">
                            AP
                        </h1>
                        <p>SITUAÇÃO ALUNO</p>
                    </div>
                </section>
            </div>


        </div>
        <!--state overview end-->

    </section>
    <?php
}
?>




<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 


<!--<script>
    function countUp(count)
    {
        var div_by = 100,
                speed = Math.round(count / div_by),
                $display = $('.count'),
                run_count = 1,
                int_speed = 24;
        var int = setInterval(function () {
            if (run_count < div_by) {
                $display.text(speed * run_count);
                run_count++;
            } else if (parseInt($display.text()) < count) {
                var curr_count = parseInt($display.text()) + 1;
                $display.text(curr_count);
            } else {
                clearInterval(int);
            }
        }, int_speed);
    }

    countUp(<?php echo $totalAtual['totalatual']; ?>);
    
    
    
    
    function countUp2(count)
{
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.count2'),
        run_count = 1,
        int_speed = 24;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

countUp2(<?php echo $totalHora['chtotal'] ?>);

function countUp3(count)
{
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.count3'),
        run_count = 1,
        int_speed = 24;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

countUp3(<?php echo $totalAtividades[''] ?>);
</script>-->