<section class="wrapper">

    <div class="col-lg-6">

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> CADASTRO DE CURSO</strong></div>
            <div class="panel-body">

                <?php echo form_open('curso/add', array('enctype' => 'multipart/form-data')); ?>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome do Curso</label>
                            <input type="text" required="required" name="curso"  class="form-control"  >
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome Abrev. do Curso</label>
                            <input type="text" name="abreviatura"  class="form-control" >
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Habilitação do Curso</label>
                            <input type="text" name="habilidade" class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Horas de Estagio Obrigatorio</label>
                            <input type="text" name="estagio" class="form-control">
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Horas de Ativ. Comp. Obrigatorio</label>
                            <input type="text" name="atividades_complementares" class="form-control" >
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Duração do Curso (Semestre(s))</label>
                            <input type="text"  required="required" name="duracao" class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Coodenador(a)</label>
                            <input type="text" name="coordenador" required="required" class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Valor do Cuso</label>
                            <input type="text" name="valor" required="required" class="form-control" >
                        </div>
                    </div>
                </div>

                <hr/>


                <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>template/js/bootstrap-inputmask.min.js"></script>