<section class="wrapper">
    <div class="col-lg-6">
        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> CADASTRO DE CURSO</strong></div>
            <div class="panel-body">

                <?php echo form_open('curso/edit/'.$curso['curso_id'], array('enctype' => 'multipart/form-data')); ?>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome do Curso</label>
                            <input type="text" required="required" value="<?php echo $curso['descricao']; ?>"  name="curso"  class="form-control"  >
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome Abrev. do Curso</label>
                            <input type="text" name="abreviatura" value="<?php echo $curso['abreviatura']; ?>"  class="form-control" >
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Habilitação do Curso</label>
                            <input type="text" name="habilidade" value="<?php echo $curso['habilitacao']; ?>"  class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Horas de Estagio Obrigatorio</label>
                            <input type="text" name="estagio" value="<?php echo $curso['estagio_obrigatoria']; ?>"  class="form-control">
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Horas de Ativ. Comp. Obrigatorio</label>
                            <input type="text" name="atividades_complementares" value="<?php echo $curso['ativ_comp_obrigatoria']; ?>"  class="form-control" >
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Duração do Curso (Semestre(s))</label>
                            <input type="text"  required="required" name="duracao" value="<?php echo $curso['duracao']; ?>"  class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Coodenador(a)</label>
                            <input type="text" name="coordenador" required="required" value="<?php echo $curso['coordenador']; ?>"  class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Valor do Cuso</label>
                            <input type="text" name="valor" required="required" value="<?php echo $curso['valor']; ?>"  class="form-control" >
                        </div>
                    </div>
                </div>

                <hr/>


                <input type="submit" class="btn btn-primary" value="SALVAR ALTERAÇÕES"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>template/js/bootstrap-inputmask.min.js"></script>