<section class="wrapper">

    <div class="col-lg-6">

        
            <?php if ($this->session->flashdata('message') != ""): ?>

                <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <?php echo $this->session->flashdata('message'); ?>

                </div>
            <?php endif; ?>
        
        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> DADOS INSTITUCIONAIS</strong></div>
            <div class="panel-body">

                <?php echo form_open('instituicao/edit', array('enctype' => 'multipart/form-data')); ?>
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome Instituição</label>
                            <input type="text" name="nome" value='<?php echo $dados['nome']; ?>' required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>CH Atividade Complemantares</label>
                            <input type="text" name="ch" value='<?php echo $dados['ch']; ?>' required="required" class="form-control"  required="required">
                        </div>
                    </div>
                </div>

                
                
                  <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Média</label>
                            <input type="text" name="media" value='<?php echo $dados['media']; ?>' required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Endereço</label>
                            <input type="text" name="endereco" value='<?php echo $dados['endereco']; ?>'required="required" class="form-control"  required="required">
                        </div>
                    </div>
                </div>
                
                
                  <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Observações</label>
                            <textarea name='obs' class='form-control' rows="7"><?php echo $dados['obs']; ?></textarea>
                        </div>
                    </div>

                </div>


                <hr/>


                <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 