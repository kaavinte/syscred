<section class="wrapper site-min-height">

    <h1 style="font-weight: 300;"><span class="fa   fa-pencil-square"></span> ATIVIDADES E HORAS COMPLEMENTARES</h1>
    <hr style="border: 1px solid #333;">
    <div class="divider"></div>
    <div class="divider"></div>



    <div class="row">
        <div class="col-lg-12">

            <?php if ($this->session->flashdata('message') != ""): ?>

                <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <?php echo $this->session->flashdata('message'); ?>

                </div>
            <?php endif; ?>

            <section class="panel">

                <header class="panel-heading">
                    <a href="<?php echo base_url(); ?>Atividades_Horas/add"><button class="btn btn-primary"><span class="glyphicon glyphicon-plus">
                            </span> ATIVIDADE</button>
                    </a>
                </header>



                <div class="panel-body">
                    <div class="adv-table" style="overflow-x: auto">

                        <table  class="display table table-bordered table-striped" id="example" style="font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nome Atividade</th>
                                    <th>Data Início</th>
                                    <th>Data Término</th>
                                    <th>CH</th>
                                    <th>AÇÕES</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cont = 1;
                                foreach ($creditos as $row):
                                    ?>
                                    <tr>
                                        <td><?php echo $cont++; ?></td>
                                        <td><?php echo $row['nome_atividade']; ?></td>
                                        <td>
                                            <?php
                                            $dataInicio = explode("-", $row['data_inicio']);
                                            echo $dataInicio[2] . "/" . $dataInicio[1] . "/" . $dataInicio[0];
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $dataTermino = explode("-", $row['data_termino']);
                                            echo $dataTermino[2] . "/" . $dataTermino[1] . "/" . $dataTermino[0];
                                            ?>
                                        </td>

                                        <td><?php echo $row['carga_horaria']; ?></td>
                                        <td style="width: 50px; text-align: center">
                                            <a href="<?php echo base_url(); ?>Atividades_Horas/edit/<?php echo $row['credito_id']; ?>"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-refresh"></i> Editar</button></a>
                                            <a href="<?php echo base_url(); ?>Atividades_Horas/delete/<?php echo $row['credito_id']; ?>"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Excluir</button></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>



                            </tbody>

                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/assets/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').dataTable({
            "aaSorting": [[6, "desc"]]
        });
    });
</script>