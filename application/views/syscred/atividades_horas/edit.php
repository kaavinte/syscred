<section class="wrapper">

    <div class="col-lg-6">

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> CADASTRO DE ATIVIDADES E HORAS COMPLEMENTARES</strong></div>
            <div class="panel-body">

                <?php echo form_open('Atividades_Horas/edit/'.$horas['credito_id'], array('enctype' => 'multipart/form-data')); ?>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Nome Atividade</label>
                            <input type="text" name="nome_atividade" value="<?php echo $horas['nome_atividade']; ?>" required="required" class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Data Inicio</label>
                            <input class="form-control" data-mask="99/99/9999"

                                   value="<?php
                                   $data = explode('-', $horas['data_inicio']);
                                   $data = $data[2] . "/" . $data[1] . "/" . $data[0];
                                   echo $data;
                                   ?>"


                                   type="text" name="data_inicio" required="required" class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Data termino</label>
                            <input type="text" class="form-control" data-mask="99/99/9999" 


                                   value="<?php
                                   $dataTermino = explode('-', $horas['data_termino']);
                                   $dataTermino = $dataTermino[2] . "/" . $dataTermino[1] . "/" . $dataTermino[0];
                                   echo $data;
                                   ?>"name="data_termino" required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Carga Horaria</label>
                            <input type="text" name="carga_horaria" value="<?php echo $horas['carga_horaria']; ?>" required="required" class="form-control" >
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Local</label>
                            <input type="text" name="local" value="<?php echo $horas['local']; ?>" required="required" class="form-control">
                        </div>
                    </div>
                </div>


                <hr/>


                <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>template/js/bootstrap-inputmask.min.js"></script>