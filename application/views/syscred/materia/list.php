<section class="wrapper">

    <div class="col-lg-12">

        <?php if ($this->session->flashdata('message') != ""): ?>

            <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <?php echo $this->session->flashdata('message'); ?>

            </div>
        <?php endif; ?>

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> MATÉRIAS</strong></div>
            <div class="panel-body">

                <section class="panel tasks-widget">

                    <div class="panel-body">

                        <table style="font-size: 12px;" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nº</th>

                                    <th>Disciplina</th>
                                    <th>Série</th>
                                    <th>Crédito</th>
                                    <th>C.H. Teor</th>
                                    <th>C.H. Prát</th>
                                    <th>C.H. Outros</th>
                                    <th>C.H. Total</th>
                                    <th style='text-align: center;'>APROVADO</th>
                                    <th style='text-align: center;'>REPROVADO</th>
                                    <th style='text-align: center;'>REPROV. FALTA</th>
                                    <th style='text-align: center;'>STATUS</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cont = 1;
                                $cont2 = 1;
                                $cont3 = 1;
                                $cont4 = 1;
                                foreach ($matriz as $row):
                                    ?>
                                    <tr>
                                        <td style='width: 5%'><?php echo $cont++; ?></td>

                                        <td><?php echo $row['disciplina']; ?></td>

                                        <td><?php echo $row['serie']; ?></td>
                                        <td><?php echo $row['credito']; ?></td>
                                        <td><?php echo $row['ch']; ?></td>
                                        <td><?php echo $row['chp']; ?></td>
                                        <td><?php echo $row['cho']; ?></td>
                                        <td><?php echo $row['chtotal']; ?></td>


                                <input type='hidden' value='<?php echo $row['matriz_disciplina_id']; ?>' name='id<?php echo $cont4++; ?>'/>


                                <td style='text-align: center;'>
                                    <a href='<?php echo base_url(); ?>materia/situacao/<?php echo $row['situacao_matriz_disciplina_id']; ?>/1'><button class="btn btn-default btn-xs"><i class=" fa fa-check"></i></button></a>
                                </td>

                                <td style='text-align: center;'>
                                    <a href='<?php echo base_url(); ?>materia/situacao/<?php echo $row['situacao_matriz_disciplina_id']; ?>/2'><button class="btn btn-default btn-xs"><i class=" fa fa-check"></i></button></a>

                                </td>

                                <td style='text-align: center;'>
                                    <a href='<?php echo base_url(); ?>materia/situacao/<?php echo $row['situacao_matriz_disciplina_id']; ?>/3'><button class="btn btn-default btn-xs"><i class=" fa fa-check"></i></button></a>

                                </td>


                                <?php
                                if ($row['situacao'] == 0) {
                                    ?>
                                    <td style='text-align: center;'>

                                    </td>  

                                    <?php
                                } else if ($row['situacao'] == 1) {
                                    ?>
                                    <td  style='text-align: center;'>
                                        <button style='width: 100px;' type="button" class="btn btn-success btn-sm">APROVADO</button>
                                    </td>     
                                    <?php
                                } else if ($row['situacao'] == 2) {
                                    ?>
                                    <td style='text-align: center;'>
                                        <button style='width: 100px;' type="button" class="btn btn-danger btn-sm">REPROVADO</button>
                                    </td>     
                                    <?php
                                } else if ($row['situacao'] == 3) {
                                    ?>

                                    <td style='text-align: center;'>
                                        <button style='width: 100px;' type="button" class="btn btn-warning btn-sm">REPROV. FALTA</button>
                                    </td>  

                                    <?php
                                }
                                ?>





                                </tr>
                                <?php
                            endforeach;
                            ?>
                            </tbody>


                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>TOTAL DE HORAS E CRÉDITOS</th>
                                    <th><?php echo $totalAtual['totalatual']; ?></th>
                                    <th><?php echo $totalHora['chtotal']; ?></th>
                                    <th><?php echo $totalHoraP['chp']; ?></th>
                                    <th><?php echo $totalHoraO['cho']; ?></th>
                                    <th><?php echo $totalTotal['chtotal']; ?></th>
                                    <th></th>
                                    <th ></th>
                                    <th> </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>


                        </table>


                    </div>
                </section>

                <hr/>

                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>template/js/bootstrap-inputmask.min.js"></script>