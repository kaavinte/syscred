<section class="wrapper">

    <div class="col-lg-6">

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> CADASTRO DE MATÉRIA</strong></div>
            <div class="panel-body">

                <?php echo form_open('materia/add', array('enctype' => 'multipart/form-data')); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" name="nome" required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                </div>



                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Professor</label>
                            <input type="text" name="professor" required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Ano</label>
                            <input type="text" name="ano" required="required" class="form-control"  required="required">
                        </div>
                    </div>
                </div>


                <hr/>


                <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 