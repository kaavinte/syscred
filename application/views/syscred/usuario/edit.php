<section id="main-content">
    <section class="wrapper">

        <div class="col-lg-6">

            <section class="panel">

                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> CADASTRO DE USUÁRIO</strong></div>
                <div class="panel-body">

                    <?php foreach ($updateUsuario as $row): ?>
                        <?php echo form_open('usuario/usuario/update/'.$row['usuario_id'], array('enctype' => 'multipart/form-data')); ?>

                        <div class="form-group">
                            <label for="exampleInputEmail1">NOME</label>
                            <input name="nome" id="nome"
                                   placeholder = "seu nome completo"
                                   data-ng-minlength = "2"
                                   data-ng-maxlength = "200" 
                                   required
                                   value="<?php echo $row['usuario']; ?>"
                                   type="text" 
                                   class="form-control" 
                                   data-placement="top">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">LOGIN</label>
                            <input name="login" id="login"
                                   placeholder = "escolha um login"
                                   data-ng-minlength = "2"
                                   data-ng-maxlength = "12" 
                                   required
                                   type="text" 
                                   value="<?php echo $row['login']; ?>"
                                   class="form-control" 
                                   data-placement="top">
                        </div>

                        <div class = "form-group">
                            <label for="exampleInputEmail1">SENHA</label>
                            <input type = "password"
                                   name = "senha"
                                   value="<?php echo $row['senha']; ?>"
                                   id = "senha"
                                   required
                                   class = "form-control">

                        </div>
                        <div class = "form-group">
                            <label for = "">CONFIRME A SENHA</label>
                            <input type = "password"
                                   name= "password"
                                   value="<?php echo $row['senha']; ?>"
                                   required
                                   class = "form-control">

                        </div>

                        </br>

                        <hr/>

                        <input type="submit" class="btn btn-primary" value="ATUALIZAR"></input>
                        <?php echo form_close(); ?>
                    <?php endforeach; ?>
                </div>
            </section>
        </div>

    </section>
</section>