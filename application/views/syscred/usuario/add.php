<section id="main-content">
    <section class="wrapper">

        <div class="col-lg-6">

            <section class="panel">

                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> CADASTRO DE USUÁRIO</strong></div>
                <div class="panel-body">

                    <?php echo form_open('usuario/usuario/create', array('enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="exampleInputEmail1">NOME</label>
                        <input name="nome" id="nome"
                               placeholder = "seu nome completo"
                               data-ng-minlength = "2"
                               data-ng-maxlength = "200" 
                               required
                               type="text" 
                               class="form-control" 
                               data-placement="top">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">LOGIN</label>
                        <input name="login" id="login"
                               placeholder = "escolha um login"
                               data-ng-minlength = "2"
                               data-ng-maxlength = "12" 
                               required
                               type="text" 
                               class="form-control" 
                               data-placement="top">
                    </div>

                    <div class = "form-group">
                        <label for="exampleInputEmail1">SENHA</label>
                        <input type = "password"
                               name = "senha"
                               id = "senha"
                               required
                               class = "form-control">

                    </div>
                    <div class = "form-group">
                        <label for = "">CONFIRME A SENHA</label>
                        <input type = "password"
                               name= "password"
                               required
                               class = "form-control">

                    </div>

                    </br>

                    <hr/>

                    <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                    <?php echo form_close(); ?>

                </div>
            </section>
        </div>

    </section>
</section>