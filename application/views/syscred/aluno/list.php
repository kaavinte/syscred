<section class="wrapper site-min-height">

<!--    <h1 style="font-weight: 300;"><span class="fa   fa-pencil-square"></span> LISTA DE ALUNO(S)</h1>
    <hr style="border: 1px solid #333;">-->
    <div class="divider"></div>
    <div class="divider"></div>



    <div class="row">
        <div class="col-lg-12">

            <?php if ($this->session->flashdata('message') != ""): ?>

                <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <?php echo $this->session->flashdata('message'); ?>

                </div>
            <?php endif; ?>

            <section class="panel">

                <!--                <header class="panel-heading">
                                    <a href="<?php echo base_url(); ?>aluno/add"><button class="btn btn-primary"><span class="glyphicon glyphicon-check">
                                            </span> ALTERAR DADOS</button>
                                    </a>
                                </header>-->



                <div class="panel-body">
                    <div class="adv-table" style="overflow-x: auto">



                        <section class="panel">
                            <header class="panel-heading">
                                DADOS DO ALUNO
                            </header>
                            <table class="table" style="font-size: 13px;">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Matrícula</th>
                                        <th>Email</th>
                                       
                                        <th>AÇÕES</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $cont = 1;
                                    foreach ($alunos as $row):
                                        ?>

                                        <tr class="gradeX">
                                            <td><?php echo $cont++; ?></td>
                                            <td><?php echo $row['nome']; ?></td>
                                            <td><?php echo $row['matricula']; ?></td>
                                            <td><?php echo $row['email']; ?></td>
                                            <td text-align: center;" >
                                                <a href="<?php echo base_url(); ?>aluno/edit/<?php echo $row['aluno_id']; ?>">
                                                    <button type="button" class="btn btn-info btn-xs"><i class="fa fa-refresh"></i> Alterar Dados Cadastrais</button>
                                                </a>
                                            </td>
                                        </tr>


                                        <?php
                                    endforeach;
                                    ?>

                                </tbody>
                            </table>
                        </section>



                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/assets/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').dataTable({
            "aaSorting": [[6, "desc"]]
        });
    });
</script>