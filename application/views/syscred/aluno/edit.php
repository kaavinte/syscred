<section class="wrapper">

    <div class="col-lg-6">

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> ALTERAR DADOS CADASTRAIS</strong></div>
            <div class="panel-body">

                <?php echo form_open('aluno/edit/' . $aluno['aluno_id'], array('enctype' => 'multipart/form-data')); ?>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome Aluno</label>
                            <input type="text" value="<?php echo $aluno['nome']; ?>" name="nome" required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Matricula</label>
                            <input type="text" value="<?php echo $aluno['matricula']; ?>" name="matricula" required="required" class="form-control"  required="required">
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="text" name="email" value="<?php echo $aluno['email']; ?>" required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Senha</label>
                            <input type="password" value="<?php echo $aluno['senha']; ?>" name="senha" required="required" class="form-control"  required="required">
                        </div>
                    </div>
                </div>


                <hr/>
                <header class="panel-heading">
                    INFORMAÇÕES ADICIONAIS
                </header>

                <br/>



                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Matriz</label>

                            <select name='matriz_id' class='form-control'>
                                <option value=''>Selecione</option>

                                <?php
                                foreach ($matriz as $row):
                                    ?>
                                    <option 

                                        <?php
                                        if ($aluno['matriz_id'] == $row['matriz_id']) {

                                            echo "selected='true'";
                                        }
                                        ?>

                                        value='<?php echo $row['matriz_id'] ?>'><?php echo $row['nome']; ?></option>
                                        <?php
                                    endforeach;
                                    ?>
                            </select>

                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>CPF</label>
                            <input type="text" value="<?php echo $aluno['cpf']; ?>" name="cpf" class="form-control" id="razaosocial" >
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>RG</label>
                            <input type="text" value="<?php echo $aluno['rg']; ?>" name="rg" class="form-control">
                        </div>
                    </div>
                </div>





                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Orgão Expedidor</label>
                            <input type="text" name="orgao" value="<?php echo $aluno['orgao']; ?>" class="form-control" id="razaosocial" >
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Gênero</label>

                            <select name="genero" class="form-control">
                                <option value="">Selecione</option>
                                <option <?php
                                if ($aluno['genero'] == 1) {
                                    echo 'selected="true"';
                                }
                                ?> value="1">Masculino</option>
                                <option <?php
                                if ($aluno['genero'] == 2) {
                                    echo 'selected="true"';
                                }
                                ?>  value="2">Feminino</option>
                            </select>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Naturalidade</label>
                            <input type="text" value="<?php echo $aluno['naturalidade']; ?>" name="naturalidade"  class="form-control">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nacionalidade</label>
                            <input type="text" value="<?php echo $aluno['nacionalidade']; ?>" name="nacionalidade" class="form-control">
                        </div>
                    </div>
                </div>




                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Endereço</label>
                            <input type="text" value="<?php echo $aluno['endereco']; ?>" name="endereco" r class="form-control">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Cidade</label>
                            <input type="text" name="cidade" value="<?php echo $aluno['cidade']; ?>" class="form-control">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>UF</label>
                            <input type="text" value="<?php echo $aluno['uf']; ?>" name="uf"  class="form-control">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>CEP</label>
                            <input type="text" value="<?php echo $aluno['cep']; ?>" name="cep"  class="form-control">
                        </div>
                    </div>
                </div>


                <hr/>



                <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 