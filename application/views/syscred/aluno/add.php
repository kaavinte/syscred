<section class="wrapper">

    <div class="col-lg-6">

        <section class="panel">

            <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> CADASTRO DE ALUNO</strong></div>
            <div class="panel-body">

                <?php echo form_open('aluno/add', array('enctype' => 'multipart/form-data')); ?>
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome Aluno</label>
                            <input type="text" name="nome" required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Matricula</label>
                            <input type="text" name="matricula" required="required" class="form-control"  required="required">
                        </div>
                    </div>
                </div>

                
                
                  <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="text" name="email" required="required" class="form-control" id="razaosocial" required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Senha</label>
                            <input type="password" name="senha" required="required" class="form-control"  required="required">
                        </div>
                    </div>
                </div>


                <hr/>


                <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                <?php echo form_close(); ?>

            </div>
        </section>
    </div>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>template/js/jquery-1.8.3.min.js"></script> 