<section class="wrapper site-min-height">

<!--    <h1 style="font-weight: 300;"><span class="fa   fa-pencil-square"></span> LISTA DE ALUNO(S)</h1>
    <hr style="border: 1px solid #333;">-->
    <div class="divider"></div>
    <div class="divider"></div>



    <div class="row">
        <div class="col-lg-12">

            <?php if ($this->session->flashdata('message') != ""): ?>

                <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <?php echo $this->session->flashdata('message'); ?>

                </div>
            <?php endif; ?>

            <section class="panel">

                <!--                <header class="panel-heading">
                                    <a href="<?php echo base_url(); ?>aluno/add"><button class="btn btn-primary"><span class="glyphicon glyphicon-check">
                                            </span> ALTERAR DADOS</button>
                                    </a>
                                </header>-->



                <div class="panel-body">
                    <div class="adv-table" style="overflow-x: auto">

                        <section class="panel">
                            <header class="panel-heading">
                                LISTA DE ALUNO (S)
                            </header>
                            <table style="font-size: 12px;" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nome</th>
                                        <th>Matricula</th>
                                        <th>STATUS</th>
                                        <th>AÇÃO</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $cont = 1;
                                    $cont2 = 1;
                                    foreach ($alunos as $row):
                                        ?>
                                        <tr>
                                            <td><?php echo $cont++; ?></td>
                                            <td><?php echo $row['nome'] ?></td>
                                            <td><?php echo $row['matricula'] ?></td>
                                            <td>

                                                <?php
                                                if ($row['status'] == 1) {
                                                    ?>

                                                    <button style="width: 90px;" type="button" class="btn btn-success btn-sm">ATIVO</button>
                                                    <?php
                                                } else {
                                                    ?>

                                                    <button style="width: 90px;" type="button" class="btn btn-danger btn-sm">DESATIVADO</button>

                                                    <?php
                                                }
                                                ?>



                                            </td>
                                            <td>

                                                <a href="<?php echo base_url(); ?>aluno/MudaStatus/<?php echo $row['aluno_id']; ?>">
                                                    <div class="switch switch-square"
                                                         data-on-label="<i class=' fa fa-check'></i>"
                                                         data-off-label="<i class='fa fa-times'></i>">
                                                        <input name="confirmar" value="1" type="checkbox" 

                                                               <?php
                                                               if ($row['status'] == 1) {
                                                                   echo "checked";
                                                               }
                                                               ?>


                                                               />

                                                    </div>
                                                </a> 

                                            </td>
                                        </tr>

                                        <?php
                                    endforeach;
                                    ?>
                                </tbody>




                            </table>
                        </section>



                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>template/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/assets/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').dataTable({
            "aaSorting": [[6, "desc"]]
        });
    });
</script>