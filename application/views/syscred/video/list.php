<section id="main-content">
    <section class="wrapper site-min-height">

        <h1 style="font-weight: 300;"><span class="fa   fa-pencil-square"></span> LISTA DE VÍDEO</h1>
        <hr style="border: 1px solid #333;">
        <div class="divider"></div>
        <div class="divider"></div>

        <div class="row">
            <div class="col-lg-12">

                <?php if ($this->session->flashdata('message') != ""): ?>

                    <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <?php echo $this->session->flashdata('message'); ?>

                    </div>
                <?php endif; ?>

                <section class="panel">

                    <header class="panel-heading">
                        <a href="<?php echo base_url(); ?>video/add"><button class="btn btn-primary"><span class="glyphicon glyphicon-plus">
                                </span> VÍDEO</button>
                        </a>
                    </header>



                    <div class="panel-body">
                        <div class="adv-table">

                            <table  style="font-size: 12px;"  class="display table table-bordered table-striped text-center" id="example">
                                <thead>
                                    <tr>
                                        <th>TÍTULO</th>
                                        <th>TIPO </th>
                                        <th style="text-align: center;">POSTADO POR</th>
                                        <th style="text-align: center;">AÇÕES</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $cont2 = 1;
                                    $cont = 1;

                                    foreach ($dadosVideo as $row):
                                        ?>
                                        <tr class="gradeA">
                                            <td><?php echo $row['titulo']; ?></td>
                                            <td>
                                                <?php
                                                if ($row['tipo'] == 1) {
                                                    echo "AO VIVO";
                                                } else if ($row['tipo'] == 2) {
                                                    echo "NÃO";
                                                }
                                                ?>

                                            </td>
                                            <td><?php echo $row['usuario'] ?></td>

                                            <td style='text-align: center;'>
                                                <a href="<?php echo base_url(); ?>video/update/<?php echo $row['video_id']; ?>"> <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                                <a data-toggle="modal" href="#myModal2<?php echo $cont++; ?>">  <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                            </td>

                                        </tr>

                                    <div class="modal fade" id="myModal2<?php echo $cont2++; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Excluir Vídeo</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Deseja realmente excluir este Vídeo?
                                                </div>
                                                <div class="modal-footer">
                                                    <button data-dismiss="modal" class="btn btn-default" type="button">Fechar</button>
                                                    <a href="<?php echo base_url(); ?>/video/video/delete/<?php echo $row['video_id']; ?>"><button class="btn btn-warning" type="button"> Confirmar</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                endforeach;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').dataTable({
            "aaSorting": [[4, "desc"]]
        });
    });
</script>