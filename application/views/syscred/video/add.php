<section id="main-content">
    <section class="wrapper">

        <div class="col-lg-6">

            <section class="panel">

                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> CADASTRO DE VÍDEO</strong></div>
                <div class="panel-body">

                    <?php echo form_open('video/video/create', array('enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="exampleInputEmail1">TÍTULO DO VÍDEO</label>
                        <input name="titulo" id="nome" tooltip="Título do seu artigo, EVITE TÍTULOS LONGOS" type="text" class="form-control" data-placement="top">
                    </div>

                    <div class="col-sm-12">
                        <label for="exampleInputEmail1">CÓDIGO DO VÍDEO NO YOUTUBE</label>
                        <div class="input-group">
                            <span class="input-group-addon">https://www.youtube.com/watch?v=</span>
                            <input type="text" name="codigo" class="form-control tooltips" data-placement="top" data-original-title="Poste o vídeo no youtube, copie o código de 11 dígitos gerado no link do vídeo após o sinal de igual (=), conforme o modelo abaixo" placeholder="1xq0gD-DIoc">
                        </div>
                    </div>
                    <br/>

                    <hr/>

                    <div class="row ">
                      
                        <div class="col-sm-12">

                            <hr/>
                            <label style="margin-right: 20px;" class="col-sm-3 col-sm-3">AO VIVO?</label>
                            <div class="switch switch-square"
                                 data-off-label="<i class='fa fa-times'></i>"
                                 data-on-label="<i class=' fa fa-check'></i>">
                                <input name="publicar" value="1" type="checkbox"  checked />
                            </div>

                            <hr/>

                        </div>
                    </div>

                    <input type="submit" class="btn btn-primary" value="SALVAR"></input>
                    <?php echo form_close(); ?>

                </div>
            </section>
        </div>

    </section>
</section>