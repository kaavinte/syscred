<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">


            <?php
            if ($this->session->userdata('type_login') == "admin") {
                ?>


                <li>
                    <a href="<?php echo base_url(); ?>aluno/AlunoConfirma">
                        <i class="fa fa-user"></i>
                        <span>Alunos</span>
                    </a>
                </li>


                <li>
                    <a href="<?php echo base_url(); ?>curso">
                        <i class="fa fa-edit"></i>
                        <span>Cursos</span>
                    </a>
                </li>


                <li>
                    <a  href="<?php echo base_url(); ?>matriz">
                        <i class="fa fa-tasks"></i>
                        <span>Matriz</span>
                    </a>
                </li>
                
                
                
                <li>
                    <a href="<?php echo base_url(); ?>Instituicao/edit" >
                        <i class="fa fa-tags"></i>
                        <span> Dados Institucionais </span>
                    </a>
                </li>


                <?php
            } else if ($this->session->userdata('type_login') == "aluno") {
                ?>

                <li>
                    <a href="<?php echo base_url(); ?>dashboard">
                        <i class="fa fa-dashboard"></i>
                        <span>Início</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url(); ?>aluno" >
                        <i class="fa fa-user"></i>
                        <span>Perfil </span>
                    </a>
                </li>



                <li>
                    <a href="<?php echo base_url(); ?>materia">
                        <i class="fa fa-pencil-square-o"></i>
                        <span>Materias </span>
                    </a>
                </li>


                <li>
                    <a href="<?php echo base_url(); ?>Atividades_Horas" >
                        <i class="fa fa-clock-o"></i>
                        <span> Atv. e Horas Compl. </span>
                    </a>
                </li>


                <li>
                    <a href="<?php echo base_url(); ?>credito">
                        <i class="fa fa-credit-card"></i>
                        <span> Créditos </span>
                    </a>
                </li>



                <?php
            }
            ?>









            <!--            <li>
                            <a href="<?php echo base_url(); ?>materia" >
                                <i class="fa fa-adn"></i>
                                <span>Matérias </span>
                            </a>
                        </li>-->



            <!--            <li>
                            <a href="google_maps.html" >
                                <i class="fa fa-bookmark-o"></i>
                                <span>Atividades Complementares </span>
                            </a>
                        </li>-->





        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->