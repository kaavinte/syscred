
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keyword" content="Sky admin, administrador de sites">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>template/img/favicon.png">
    <?php include 'includes_header.php'; ?>
    <title> Login | <?php echo $system_title; ?> </title>
</head>

<!--sidebar end-->

<body class="login-body">
  
       <?php if ($this->session->flashdata('message') != ""): ?>

        <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="fa fa-times"></i>
            </button>
            <?php echo $this->session->flashdata('message'); ?>

        </div>
    <?php endif; ?>
    
  <div class="container">
        
      


    <?php echo form_open('login/validate_login', array('class' => 'form-signin')); ?>

    <h2 class="form-signin-heading" style="background-image: url(<?php echo base_url(); ?>template/img/bg-topo.png);">
        <img style="margin-left: 2px; margin-bottom: -10px;" src="<?php echo base_url(); ?>template/img/syscred.png" height="50" width="230" alt="" />
    </h2>


    <div class="login-wrap">





        <input type="text" name="usuario" class="form-control" placeholder="Matrícula" autofocus>
        <input type="password" name="senha" class="form-control" placeholder="Senha">

        <label class="checkbox">
            <input type="checkbox" value="remember-me"> Lembre-me
            <span class="pull-right ">
                <a data-toggle="modal" href="#myModal"> Esqueceu a senha?</a>
            </span>
        </label>
        <button class="btn btn-lg btn-login btn-block" type="submit">Entrar</button>

        <hr/>
        <div class="registration">

            Não tem uma conta ainda?
            <a class="" href="<?php echo base_url(); ?>registro">
                Crie uma conta
            </a>
        </div>


    </div>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #312A1F;">
                    <h4 class="modal-title">Esqueceu a senha?</h4>
                </div>
                <div class="modal-body">
                    <p>coloque seu email, para mandar-mos um lembrete</p>
                    <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button">Lembrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->


    <?php echo form_close(); ?>

</div>

</body>

<script src="<?php echo base_url(); ?>template/js/jquery.js"></script>
<?php include 'includes_footer.php'; ?>
</html>
