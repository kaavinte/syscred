<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keyword" content="Sky admin, administrador de sites">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>template/img/favicon.png">
        <?php include 'includes_header.php'; ?>
        <title><?php echo $page_title; ?> | <?php echo $system_title; ?></title>
    </head>


    <body class="login-body">
        
        <?php if ($this->session->flashdata('message') != ""): ?>

                <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <?php echo $this->session->flashdata('message'); ?>

                </div>
            <?php endif; ?>

        <div class="container">
            <?php echo form_open('registro/add', array('class' => 'form-signin', 'enctype' => 'multipart/form-data')); ?>

            <h2 class="form-signin-heading">CRIAR CONTA</h2>
            <div class="login-wrap">

                <p>Insira seus dados pessoais abaixo</p>
                <input type="text" name="nome" class="form-control" required="required" placeholder="Nome" autofocus>
                <input type="text" name="matricula" class="form-control" required="required" placeholder="Matricula" autofocus>

                <select style='font-size: 13px;' name="curso_id" required="required" class="form-control">
                    <option value="">Selecione o Curso</option>

                    <?php
                    foreach ($cursos as $row):
                        ?>
                        <option value="<?php echo $row['curso_id'] ?>"><?php echo $row['descricao']; ?></option>
                        <?php
                    endforeach;
                    ?>
                </select>
                <br/>
         
                <input type="text" name="email" required="required" class="form-control" placeholder="Email" autofocus>
                <input type="password" name="senha" required="required" class="form-control" placeholder="Senha" autofocus>

                <button class="btn btn-lg btn-login btn-block" type="submit">Enviar</button>

                <div class="registration">
                    Já registrado.
                    <a class="" href="<?php echo base_url(); ?>login">
                        Login
                    </a>
                </div>
            </div>

            <?php echo form_close(); ?>

        </div>

    </body>
</html>