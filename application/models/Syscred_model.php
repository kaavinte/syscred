<?php

/**
 * Description of Syscred_model
 *
 * @author Karol Oliveira
 */
class Syscred_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function clear_cache() {

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function save($table, $dados) {

        $this->db->insert($table, $dados);
        return $this->db->insert_id();
    }

    public function ultimoId($table) {

        $this->db->select("max(inventory_id) as ultimoID");
        $this->db->from($table);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getTable($table, $order = null, $where = null, $value = null) {

        $this->db->select("*");
        $this->db->from($table);
        $this->db->order_by($table . "." . $order, "ASC");

        if ($where) {
            $this->db->where($where, $value);
        }
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getTableRow($table, $order = null) {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->order_by($table . "." . $order, "ASC");
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getJoin($table, $join, $order, $where = null, $value = null) {

        $this->db->select("*");
        $this->db->from($table);
        $this->db->join($join, $join . "." . $join . "_id =" . $table . "." . $join . "_id");
        $this->db->order_by($table . "." . $order, "asc");

        if ($where) {
            $this->db->where($where, $value);
        }

        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    public function getJoinRow($table, $join, $order, $where = null, $value = null) {

        $this->db->select("*");
        $this->db->from($table);
        $this->db->join($join, $join . "." . $join . "_id =" . $table . "." . $join . "_id");
        $this->db->order_by($table . "." . $order, "asc");

        if ($where) {
            $this->db->where($where, $value);
        }

        $query = $this->db->get();
          return $query->row_array();
    }

    public function getUpdate($table, $id) {

        $query = $this->db->get_where($table, array($table . "_id" => $id));
        return $query->row_array();
    }

    public function update($dados, $table, $id) {

        $this->db->where($table . "_id", $id);
        $this->db->update($table, $dados);
        if ($this->db->affected_rows() >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteMatriz($table, $matriz_id) {

        $this->db->where('matriz_disciplina_id', $matriz_id);
        $this->db->delete($table);
        if ($this->db->affected_rows() >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($table, $id) {

        $this->db->where($table . "_id", $id);
        $this->db->delete($table);
        if ($this->db->affected_rows() >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public function DeleteFoto($album_id) {

        $this->db->where('album_fotos_id', $album_id);
        $this->db->delete("fotos");
    }

    public function deleteAlbum($table, $album_id) {

        $this->db->where("album_id", $album_id);
        $this->db->delete($table);
    }

    public function GetWhere($table, $where, $value) {

        $this->db->select("*");
        $this->db->from($table);
        $this->db->where($where, $value);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function verificaAlbum($table, $value) {

        $this->db->from($table);
        $this->db->where('album_id', $value);
        return $this->db->count_all_results();
    }

    public function UpdateAlbum($dados, $table, $value) {

        $this->db->where("album_id", $value);
        $this->db->update($table, $dados);
    }

    public function isVarExists($table, $value, $NameField) {
        $this->db->from($table);
        $this->db->where($NameField, $value);
        return $this->db->count_all_results();
    }

    public function GetCount($table, $where = null, $value = null) {
        $this->db->from($table);

        if ($where) {
            $this->db->where($where, $value);
        }
        return $this->db->count_all_results();
    }

    public function GetSum($sum, $table, $where = null, $value = null, $and = null, $value2 = null) {

        $this->db->select($sum);
        $this->db->from($table);

        if ($where) {
            $this->db->where($where, $value);
        }
        
        if($and){
             $this->db->where($and, $value2);
        }
        
        $query = $this->db->get();
        return $query->row_array();
    }

}
