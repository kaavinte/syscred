<?php

/**
 * Description of Admin
 *
 * @author Karol Oliveira
 */
class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {


        $page_data['page_title'] = 'Criar Conta';
        $this->load->view('login_admin', $page_data);
    }

}
