<?php

/**
 * Description of Curso
 *
 * @author Karol Oliveira
 */
class Curso extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    }

    public function index() {

        $page_data['cursos'] = $this->syscred_model->getTable('curso', 'descricao');
        $page_data['page_name'] = 'cursos/list';
        $page_data['page_title'] = 'Lista de Curso(s)';
        $this->load->view('index', $page_data);
    }

    public function add() {

        if ($this->input->post()) {

            $data['descricao'] = $this->input->post('curso');
            $data['abreviatura'] = $this->input->post('abreviatura');
            $data['coordenador'] = $this->input->post('coordenador');
            $data['duracao'] = $this->input->post('duracao');
            $data['ativ_comp_obrigatoria'] = $this->input->post('atividades_complementares');
            $data['estagio_obrigatoria'] = $this->input->post('estagio');
            $Valor_maskara = str_replace(',', '.', str_replace('.', '', $this->input->post('valor')));
            $data['valor'] = $Valor_maskara;
            $data['habilitacao'] = $this->input->post('habilidade');


            if ($this->syscred_model->save('curso', $data)) {
                $this->session->set_flashdata('message', '<strong>CURSO</strong> cadastrado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'curso');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao cadastrar CURSO!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'curso');
            }
        } else {

            $page_data['page_name'] = 'cursos/add';
            $page_data['page_title'] = 'Cadastrar Curso';
            $this->load->view('index', $page_data);
        }
    }

    public function edit($param1 = '') {


        if ($this->input->post()) {

            $data['descricao'] = $this->input->post('curso');
            $data['abreviatura'] = $this->input->post('abreviatura');
            $data['coordenador'] = $this->input->post('coordenador');
            $data['duracao'] = $this->input->post('duracao');
            $data['ativ_comp_obrigatoria'] = $this->input->post('atividades_complementares');
            $data['estagio_obrigatoria'] = $this->input->post('estagio');
            $Valor_maskara = str_replace(',', '.', str_replace('.', '', $this->input->post('valor')));
            $data['valor'] = $Valor_maskara;
            $data['habilitacao'] = $this->input->post('habilidade');


            if ($this->syscred_model->update($data, 'curso', $param1)) {
                $this->session->set_flashdata('message', '<strong>CURSO</strong> alterado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'curso');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao alterar CURSO!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'curso');
            }
        } else {
            $page_data['curso'] = $this->syscred_model->getUpdate('curso', $param1);
            $page_data['page_name'] = 'cursos/edit';
            $page_data['page_title'] = 'Editar Curso';
            $this->load->view('index', $page_data);
        }
    }

    public function delete($param1 = '') {

        $result = $this->syscred_model->delete('curso', $param1);

        if ($result) {
            $this->session->set_flashdata('message', '<strong>CURSO</strong> excluido com sucesso!');
            $this->session->set_flashdata('type', 'warning');
            redirect(base_url() . 'curso/');
        } else {
            $this->session->set_flashdata('message', '<strong>ERRO</strong> ao excluir CURSO!');
            $this->session->set_flashdata('type', 'error');
            redirect(base_url() . 'curso/');
        }
    }

}
