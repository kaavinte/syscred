<?php

/**
 * Description of Credito
 *
 * @author Karol Oliveira
 */
class Credito extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    }

    public function index() {

        $page_data['totalcredito'] = $this->syscred_model->GetSum('SUM(credito) as credito', 'matriz_disciplina', 'matriz_id', $this->session->userdata('matriz'));
        $page_data['totalAtual'] = $this->syscred_model->GetSum('sum(credito) as totalatual', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        $page_data['totalAtividades'] = $this->syscred_model->GetSum('sum(carga_horaria) as cargaTotal', 'credito', 'aluno_id', $this->session->userdata('login'));
       
        $page_data['cursos'] = $this->syscred_model->getJoinRow('aluno', 'curso', 'nome', 'aluno_id', $this->session->userdata('login'));
       
        $page_data['page_name'] = 'credito/list';
        $page_data['page_title'] = 'Lista Aluno(s)';
        $this->load->view('index', $page_data);
    }

}
