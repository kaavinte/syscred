<?php

/**
 * Description of Materia
 *
 * @author Karol Oliveira
 */
class Materia extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    }

    public function index() {

        $page_data['matriz'] = $this->syscred_model->getTable('situacao_matriz_disciplina', 'serie', 'aluno_id', $this->session->userdata('login'));
        $page_data['totalAtual'] = $this->syscred_model->GetSum('sum(credito) as totalatual', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        
        $page_data['totalHora'] = $this->syscred_model->GetSum('sum(ch) as chtotal', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        $page_data['totalHoraP'] = $this->syscred_model->GetSum('sum(chp) as chp', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        $page_data['totalHoraO'] = $this->syscred_model->GetSum('sum(cho) as cho', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        $page_data['totalTotal'] = $this->syscred_model->GetSum('sum(chtotal) as chtotal', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        
        
        
        $page_data['SumCredito'] = $this->syscred_model->GetSum('SUM(credito) as credito', 'matriz_disciplina', 'matriz_id', $this->session->userdata('matriz'));
        $page_data['SumCht'] = $this->syscred_model->GetSum('SUM(ch) as cht', 'matriz_disciplina', 'matriz_id', $this->session->userdata('matriz'));
        $page_data['Sump'] = $this->syscred_model->GetSum('SUM(chp) as chp', 'matriz_disciplina', 'matriz_id', $this->session->userdata('matriz'));
        $page_data['Sumo'] = $this->syscred_model->GetSum('SUM(cho) as cho', 'matriz_disciplina', 'matriz_id', $this->session->userdata('matriz'));
        $page_data['SumTotal'] = $this->syscred_model->GetSum('SUM(chtotal) as total', 'matriz_disciplina', 'matriz_id', $this->session->userdata('matriz'));
        $page_data['page_name'] = 'materia/list';
        $page_data['page_title'] = 'Lista Matéria(s)';
        $this->load->view('index', $page_data);
    }

    public function add() {

        if ($this->input->post()) {
            if ($this->syscred_model->save('materia', $this->input->post())) {
                $this->session->set_flashdata('message', '<strong>MATÉRIA</strong> cadastrado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'materia');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao cadastrar MATÉRIA!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'materia');
            }
        } else {

            $page_data['page_name'] = 'materia/add';
            $page_data['page_title'] = 'Novo Matéria';
            $this->load->view('index', $page_data);
        }
    }

    public function situacao($param1 = '', $param2 = '') {


        $dados = array(
            "situacao" => $param2,
        );

        if ($this->syscred_model->update($dados, 'situacao_matriz_disciplina', $param1)) {
            $this->session->set_flashdata('message', '<strong>OPÇOES</strong> alteradas com sucesso!');
            $this->session->set_flashdata('type', 'success');
            redirect(base_url() . 'materia');
        } else {

            $this->session->set_flashdata('message', '<strong>ERRO</strong> ao alterar OPÇOES!');
            $this->session->set_flashdata('type', 'warning');
            redirect(base_url() . 'materia');
        }
    }

}
