<?php

/**
 * Description of Matriz
 *
 * @author Karol Oliveira
 */
class Matriz extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    }

    public function index() {

        $page_data['matriz'] = $this->syscred_model->getJoin('matriz', 'curso', 'matriz_id');
        $page_data['page_name'] = 'matriz/list';
        $page_data['page_title'] = 'Lista de Matriz';
        $this->load->view('index', $page_data);
    }

    public function add() {

        if ($this->input->post()) {

            if ($this->syscred_model->save('matriz', $this->input->post())) {
                $this->session->set_flashdata('message', '<strong>MATRIZ</strong> cadastrada com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'matriz');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao cadastrar MATRIZ!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'matriz');
            }
        } else {
            $page_data['cursos'] = $this->syscred_model->getTable('curso', 'descricao');
            $page_data['page_name'] = 'matriz/add';
            $page_data['page_title'] = 'Cadastrar Matriz';
            $this->load->view('index', $page_data);
        }
    }

    public function delete($param1 = '') {

        $result = $this->syscred_model->deleteMatriz('matriz_disciplina', $param1);

        if ($result) {

            $result2 = $this->syscred_model->delete('matriz', $param1);

            if ($result2) {
                $this->session->set_flashdata('message', '<strong>MATRIZ</strong> excluida com sucesso!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'Matriz');
            }
        } else {
            $this->session->set_flashdata('message', '<strong>ERRO</strong> ao excluir MATRIZ!');
            $this->session->set_flashdata('type', 'error');
            redirect(base_url() . 'Matriz');
        }
    }

    public function edit($param1 = '') {

        if ($this->input->post()) {

            if ($this->syscred_model->update($this->input->post(), 'matriz', $param1)) {
                $this->session->set_flashdata('message', '<strong>MATRIZ</strong> alterada com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'matriz');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao alterar MATRIZ!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'matriz');
            }
        } else {
            $page_data['cursos'] = $this->syscred_model->getTable('curso', 'descricao');
            $page_data['matriz'] = $this->syscred_model->getUpdate('matriz', $param1);
            $page_data['page_name'] = 'matriz/edit';
            $page_data['page_title'] = 'Editar Matriz';
            $this->load->view('index', $page_data);
        }
    }

    public function disciplinas($param1 = '') {

        $page_data['matriz'] = $this->syscred_model->getUpdate('matriz', $param1);
        $page_data['disciplinas'] = $this->syscred_model->getTable('matriz_disciplina', 'serie', 'matriz_id', $param1);
        $page_data['SumCredito'] = $this->syscred_model->GetSum('SUM(credito) as credito', 'matriz_disciplina', 'matriz_id', $param1);
        $page_data['SumCht'] = $this->syscred_model->GetSum('SUM(ch) as cht', 'matriz_disciplina', 'matriz_id', $param1);
        $page_data['Sump'] = $this->syscred_model->GetSum('SUM(chp) as chp', 'matriz_disciplina', 'matriz_id', $param1);
        $page_data['Sumo'] = $this->syscred_model->GetSum('SUM(cho) as cho', 'matriz_disciplina', 'matriz_id', $param1);
        $page_data['SumTotal'] = $this->syscred_model->GetSum('SUM(chtotal) as total', 'matriz_disciplina', 'matriz_id', $param1);



        $page_data['page_name'] = 'matriz/disciplina';
        $page_data['page_title'] = 'ADICIONANDO DISCIPLINA';
        $this->load->view('index', $page_data);
    }

    public function DiscipinaAdd($param1 = '') {

        if ($this->input->post()) {

            $total = $this->input->post('ch') + $this->input->post('chp') + $this->input->post('cho');

            $dados = array(
                "disciplina" => $this->input->post('disciplina'),
                "serie" => $this->input->post('serie'),
                "credito" => $this->input->post('credito'),
                "ch" => $this->input->post('ch'),
                "chp" => $this->input->post('chp'),
                "cho" => $this->input->post('cho'),
                "chtotal" => $total,
                "matriz_id" => $param1
            );

            if ($this->syscred_model->save('matriz_disciplina', $dados)) {
                $this->session->set_flashdata('message', '<strong>DISCIPLINA</strong> cadastrada com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'Matriz/disciplinas/' . $param1);
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao cadastrar DISCIPLINA!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'Matriz/disciplinas/' . $param1);
            }
        } else {
            $page_data['cursos'] = $this->syscred_model->getTable('curso', 'descricao');
            $page_data['page_name'] = 'matriz/add';
            $page_data['page_title'] = 'Cadastrar Matriz';
            $this->load->view('index', $page_data);
        }
    }

    public function deleteDisciplina($param1 = '', $param2 = '') {

        $result = $this->syscred_model->delete('matriz_disciplina', $param1);

        if ($result) {
            $this->session->set_flashdata('message', '<strong>DISCIPLINA</strong> excluida com sucesso!');
            $this->session->set_flashdata('type', 'warning');
            redirect(base_url() . 'Matriz/disciplinas/' . $param2);
        } else {
            $this->session->set_flashdata('message', '<strong>ERRO</strong> ao excluir DISCIPLINA!');
            $this->session->set_flashdata('type', 'error');
            redirect(base_url() . 'Matriz/disciplinas/' . $param2);
        }
    }

}
