<?php

/**
 * Description of Instituicao
 *
 * @author Karol Oliveira
 */
class Instituicao extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    }

    public function edit($param1 = '') {


        if ($this->input->post()) {

            if ($this->syscred_model->update($this->input->post(), 'instituicao', 1)) {
                $this->session->set_flashdata('message', '<strong>DADOS INSTITUCIONAIS</strong> alterado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'Instituicao/edit');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao alterar DADOS INSTITUCIONAIS!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'Instituicao/edit');
            }
        } else {
            $page_data['dados'] = $this->syscred_model->getUpdate('instituicao', 1);
            $page_data['page_name'] = 'dados/edit';
            $page_data['page_title'] = 'Dados Institucionais';
            $this->load->view('index', $page_data);
        }
    }

}
