<?php

/**
 * Description of Atividades_Horas
 *
 * @author Karol Oliveira
 */
class Atividades_Horas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    }

    public function index() {

        $page_data['creditos'] = $this->syscred_model->getTable('credito', 'credito_id', 'aluno_id', $this->session->userdata('login'));
        $page_data['page_name'] = 'atividades_horas/list';
        $page_data['page_title'] = 'Lista Atividades e Horas Complementares';
        $this->load->view('index', $page_data);
    }

    public function add() {

        if ($this->input->post()) {

            $data_inicio = explode("/", $this->input->post('data_inicio'));
            $data_inicio = $data_inicio[2] . "-" . $data_inicio[1] . "-" . $data_inicio[0];

            $data_termino = explode("/", $this->input->post('data_termino'));
            $data_termino = $data_termino[2] . "-" . $data_termino[1] . "-" . $data_termino[0];

            $dados = array(
                "nome_atividade" => $this->input->post('nome_atividade'),
                "data_inicio" => $data_inicio,
                "data_termino" => $data_termino,
                "carga_horaria" => $this->input->post('carga_horaria'),
                "local" => $this->input->post('local'),
                "aluno_id" => $this->session->userdata('login'),
            );

            if ($this->syscred_model->save('credito', $dados)) {
                $this->session->set_flashdata('message', '<strong>CRÉDITO</strong> cadastrado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'Atividades_Horas');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao cadastrar CRÉDITO!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'Atividades_Horas');
            }
        } else {

            $page_data['page_name'] = 'atividades_horas/add';
            $page_data['page_title'] = 'Cadastrar Atividades e Horas Complementares';
            $this->load->view('index', $page_data);
        }
    }

    public function delete($param1 = '') {

        $result = $this->syscred_model->delete('credito', $param1);

        if ($result) {
            $this->session->set_flashdata('message', '<strong>REGISTRO</strong> excluido com sucesso!');
            $this->session->set_flashdata('type', 'warning');
            redirect(base_url() . 'Atividades_Horas/');
        } else {
            $this->session->set_flashdata('message', '<strong>ERRO</strong> ao excluir REGISTRO!');
            $this->session->set_flashdata('type', 'error');
            redirect(base_url() . 'Atividades_Horas/');
        }
    }

    public function edit($param1 = '') {

        if ($this->input->post()) {

            $data_inicio = explode("/", $this->input->post('data_inicio'));
            $data_inicio = $data_inicio[2] . "-" . $data_inicio[1] . "-" . $data_inicio[0];

            $data_termino = explode("/", $this->input->post('data_termino'));
            $data_termino = $data_termino[2] . "-" . $data_termino[1] . "-" . $data_termino[0];

            $dados = array(
                "nome_atividade" => $this->input->post('nome_atividade'),
                "data_inicio" => $data_inicio,
                "data_termino" => $data_termino,
                "carga_horaria" => $this->input->post('carga_horaria'),
                "local" => $this->input->post('local'),
                "aluno_id" => $this->session->userdata('login'),
            );

            if ($this->syscred_model->update($dados, 'credito', $param1)) {
                $this->session->set_flashdata('message', '<strong>REGISTRO</strong> atualizado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'Atividades_Horas');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao atualizar REGISTRO!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'Atividades_Horas');
            }
        } else {
            $page_data['horas'] = $this->syscred_model->getUpdate('credito', $param1);
            $page_data['page_name'] = 'atividades_horas/edit';
            $page_data['page_title'] = 'Cadastrar Atividades e Horas Complementares';
            $this->load->view('index', $page_data);
        }
    }

}
