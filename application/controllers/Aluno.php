<?php

/**
 * Description of Aluno
 *
 * @author Karol Oliveira
 */
class Aluno extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
    }

    public function index() {

        $page_data['alunos'] = $this->syscred_model->getTable('aluno', 'nome', 'aluno_id', $this->session->userdata('login'));
        $page_data['page_name'] = 'aluno/list';
        $page_data['page_title'] = 'Lista Aluno(s)';
        $this->load->view('index', $page_data);
    }

    public function add() {

        if ($this->input->post()) {
            if ($this->syscred_model->save('aluno', $this->input->post())) {
                $this->session->set_flashdata('message', '<strong>ALUNO</strong> cadastrado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'aluno');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao cadastrar ALUNO!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'aluno');
            }
        } else {

            $page_data['page_name'] = 'aluno/add';
            $page_data['page_title'] = 'Novo Aluno';
            $this->load->view('index', $page_data);
        }
    }

    public function edit($param1 = '') {

        if ($this->input->post()) {


            $dados = array(
                "nome" => $this->input->post('nome'),
                "matricula" => $this->input->post('matricula'),
                "email" => $this->input->post('email'),
                "senha" => $this->input->post('senha'),
                "cpf" => $this->input->post('cpf'),
                "rg" => $this->input->post('rg'),
                "orgao" => $this->input->post('orgao'),
                "genero" => $this->input->post('genero'),
                "naturalidade" => $this->input->post('naturalidade'),
                "endereco" => $this->input->post('endereco'),
                "cidade" => $this->input->post('cidade'),
                "uf" => $this->input->post('uf'),
                "cep" => $this->input->post('cep'),
                "matriz_id" => $this->input->post('matriz_id'),
            );



            //VERIFICA MATRIZ

            if ($this->syscred_model->GetCount('situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login')) < 1) {

                $arrayMatriz = $this->syscred_model->getTable('matriz_disciplina', 'disciplina', 'matriz_id', $this->input->post('matriz_id'));

                foreach ($arrayMatriz as $row):

                    $dadosDisciplina = array(
                        "disciplina" => $row['disciplina'],
                        "serie" => $row['serie'],
                        "credito" => $row['credito'],
                        "ch" => $row['ch'],
                        "chp" => $row['chp'],
                        "cho" => $row['cho'],
                        "cho" => $row['cho'],
                        "chtotal" => $row['chtotal'],
                        "aluno_id" => $this->session->userdata('login'),
                    );
                    $this->syscred_model->save('situacao_matriz_disciplina', $dadosDisciplina);

                endforeach;
            }





            if ($this->syscred_model->update($dados, 'aluno', $param1)) {
                $this->session->set_flashdata('message', '<strong>ALUNO</strong> alterado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'aluno');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao alterar ALUNO!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'aluno');
            }
        } else {
            $page_data['matriz'] = $this->syscred_model->getTable('matriz', 'matriz_id');
            $page_data['aluno'] = $this->syscred_model->getUpdate('aluno', $param1);
            $page_data['page_name'] = 'aluno/edit';
            $page_data['page_title'] = 'Alterar Dados Cadastrais.';
            $this->load->view('index', $page_data);
        }
    }

    public function AlunoConfirma() {

        $page_data['alunos'] = $this->syscred_model->getTable('aluno', 'nome');
        $page_data['page_name'] = 'aluno/aprova_aluno';
        $page_data['page_title'] = 'Lista Aluno(s)';
        $this->load->view('index', $page_data);
    }

    public function MudaStatus($param1 = '') {

        $respostaStatus = $this->syscred_model->getUpdate('aluno', $param1);

        if ($respostaStatus['status'] == 0) {
            $status = 1;
        } else if ($respostaStatus['status'] == 1) {
            $status = 0;
        }

        $dados = array(
            "status" => $status,
        );

        if ($this->syscred_model->update($dados, 'aluno', $param1)) {
            $this->session->set_flashdata('message', '<strong>STATUS</strong> alterado com sucesso!');
            $this->session->set_flashdata('type', 'success');
            redirect(base_url() . 'aluno/AlunoConfirma');
        } else {

            $this->session->set_flashdata('message', '<strong>ERRO</strong> ao alterar STATUS!');
            $this->session->set_flashdata('type', 'warning');
            redirect(base_url() . 'aluno/AlunoConfirma');
        }
    }

}
