<?php

/**
 * Description of Registro
 *
 * @author Karol Oliveira
 */
class Registro extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {

////        $page_data['alunos'] = $this->syscred_model->getTable('aluno', 'nome');

        $page_data['page_title'] = 'Criar Conta';
        $page_data['cursos'] = $this->syscred_model->getTable('curso', 'descricao');
        $this->load->view('registration', $page_data);
    }

    public function add() {

        if ($this->input->post()) {
            if ($this->syscred_model->save('aluno', $this->input->post())) {
                $this->session->set_flashdata('message', '<strong>ALUNO</strong> cadastrado com sucesso!');
                $this->session->set_flashdata('type', 'success');
                redirect(base_url() . 'login');
            } else {

                $this->session->set_flashdata('message', '<strong>ERRO</strong> ao cadastrar ALUNO!');
                $this->session->set_flashdata('type', 'warning');
                redirect(base_url() . 'login');
            }
        } else {

            $page_data['page_name'] = 'aluno/add';
            $page_data['page_title'] = 'Novo Aluno';
            $this->load->view('index', $page_data);
        }
    }

}
