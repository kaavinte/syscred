<?php

/**
 * Description of Dashboard
 *
 * @author Karol Oliveira
 */
class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'login', 'refresh');
    }

    public function index() {




        $page_data['totalAtual'] = $this->syscred_model->GetSum('sum(credito) as totalatual', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        $page_data['totalHora'] = $this->syscred_model->GetSum('sum(ch) as chtotal', 'situacao_matriz_disciplina', 'aluno_id', $this->session->userdata('login'), 'situacao', 1);
        $page_data['totalAtividades'] = $this->syscred_model->GetSum('sum(carga_horaria) as cargaTotal', 'credito', 'aluno_id', $this->session->userdata('login'));
        $page_data['CountAluno'] = $this->syscred_model->GetCount('aluno');

        $page_data['page_title'] = 'Dashboard';
        $page_data['page_name'] = 'dashboard/dashboard';
        $this->load->view('index', $page_data);
    }

}
