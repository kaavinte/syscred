-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05-Dez-2016 às 07:27
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `syscred`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE IF NOT EXISTS `aluno` (
  `aluno_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `matricula` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `senha` varchar(45) NOT NULL DEFAULT '',
  `status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0-INATIVO 1-ATIVO',
  `cpf` varchar(45) NOT NULL DEFAULT '',
  `rg` varchar(45) NOT NULL DEFAULT '',
  `orgao` varchar(45) NOT NULL DEFAULT '',
  `genero` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '1-MASCULINO 2- FEMININO',
  `naturalidade` varchar(45) NOT NULL DEFAULT '',
  `nacionalidade` varchar(45) NOT NULL DEFAULT '',
  `endereco` varchar(200) NOT NULL DEFAULT '',
  `cidade` varchar(45) NOT NULL DEFAULT '',
  `uf` varchar(45) NOT NULL DEFAULT '',
  `cep` varchar(45) NOT NULL DEFAULT '',
  `matriz_id` int(10) unsigned NOT NULL DEFAULT '0',
  `curso_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aluno_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`aluno_id`, `nome`, `matricula`, `email`, `senha`, `status`, `cpf`, `rg`, `orgao`, `genero`, `naturalidade`, `nacionalidade`, `endereco`, `cidade`, `uf`, `cep`, `matriz_id`, `curso_id`) VALUES
(1, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 1, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(2, 'MARIO SILVA E SILVA', 2036587, 'mario@gmail.com', '123456', 1, '', '', '', 0, '', '', '', '', '', '', 0, 0),
(3, 'ROBERTO SILVA', 2214654, 'roberto@silva', '123456', 0, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(4, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(5, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(6, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(7, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(8, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(9, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(10, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(11, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(12, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(13, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(14, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(15, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(16, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(17, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(18, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(19, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(20, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(21, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(22, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(23, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(24, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(25, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(26, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(27, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(28, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(29, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(30, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 0, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `credito`
--

CREATE TABLE IF NOT EXISTS `credito` (
  `credito_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_atividade` varchar(200) NOT NULL DEFAULT '',
  `data_inicio` date NOT NULL DEFAULT '0000-00-00',
  `data_termino` date NOT NULL DEFAULT '0000-00-00',
  `carga_horaria` varchar(45) NOT NULL DEFAULT '',
  `local` longtext NOT NULL,
  `aluno_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`credito_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `credito`
--

INSERT INTO `credito` (`credito_id`, `nome_atividade`, `data_inicio`, `data_termino`, `carga_horaria`, `local`, `aluno_id`) VALUES
(1, 'ATIVIDADE TESTE', '2016-11-01', '2016-11-27', '20', 'AUDITÓRIO CIESA', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
  `curso_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL DEFAULT '',
  `abreviatura` varchar(45) NOT NULL DEFAULT '',
  `coordenador` varchar(45) NOT NULL DEFAULT '',
  `duracao` varchar(45) NOT NULL DEFAULT '',
  `ativ_comp_obrigatoria` varchar(45) NOT NULL DEFAULT '',
  `estagio_obrigatoria` varchar(45) NOT NULL DEFAULT '',
  `valor` double NOT NULL DEFAULT '0',
  `habilitacao` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`curso_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`curso_id`, `descricao`, `abreviatura`, `coordenador`, `duracao`, `ativ_comp_obrigatoria`, `estagio_obrigatoria`, `valor`, `habilitacao`) VALUES
(2, 'SISTEMAS DE INFORMAÇÃO', 'SIE', 'DÉRCIO SILVA', '8', '200', '200', 600, 'SI'),
(3, 'CIENCIA DA COMPUTACAO', 'CC', 'CARLA SILVA', '8', '200', '200', 600, 'CC');

-- --------------------------------------------------------

--
-- Estrutura da tabela `instituicao`
--

CREATE TABLE IF NOT EXISTS `instituicao` (
  `instituicao_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL DEFAULT '',
  `ch` varchar(45) NOT NULL DEFAULT '',
  `media` varchar(45) NOT NULL DEFAULT '',
  `endereco` varchar(45) NOT NULL DEFAULT '',
  `obs` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`instituicao_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `instituicao`
--

INSERT INTO `instituicao` (`instituicao_id`, `nome`, `ch`, `media`, `endereco`, `obs`) VALUES
(1, 'CIESA', '500', '8', 'AV DJALMA BASTISTA', 'TESTE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `matriz`
--

CREATE TABLE IF NOT EXISTS `matriz` (
  `matriz_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL DEFAULT '',
  `ano` varchar(45) NOT NULL DEFAULT '',
  `curso_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`matriz_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `matriz`
--

INSERT INTO `matriz` (`matriz_id`, `nome`, `ano`, `curso_id`) VALUES
(1, 'MATRIZ CURRICULAR VERSÃO 146', '2011', 3),
(2, 'MATRIZ CURRICULAR VERSÃO 147 ', '2012', 3),
(3, 'MATRIZ CURRICULAR VERSÃO 148 ', '2013', 3),
(4, 'MATRIZ CURRICULAR VERSÃO 149 ', '2014', 3),
(7, 'MATRIZ TESTEEEEE', '2016', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `matriz_disciplina`
--

CREATE TABLE IF NOT EXISTS `matriz_disciplina` (
  `matriz_disciplina_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL DEFAULT '',
  `disciplina` varchar(45) NOT NULL DEFAULT '',
  `serie` varchar(45) NOT NULL DEFAULT '',
  `credito` int(10) unsigned NOT NULL DEFAULT '0',
  `ch` varchar(50) NOT NULL DEFAULT '',
  `chp` varchar(45) NOT NULL DEFAULT '',
  `cho` varchar(45) NOT NULL DEFAULT '',
  `chtotal` varchar(45) NOT NULL DEFAULT '',
  `matriz_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`matriz_disciplina_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Extraindo dados da tabela `matriz_disciplina`
--

INSERT INTO `matriz_disciplina` (`matriz_disciplina_id`, `codigo`, `disciplina`, `serie`, `credito`, `ch`, `chp`, `cho`, `chtotal`, `matriz_id`) VALUES
(1, '40602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 1),
(2, '4605', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 4, '144', '0', '0', '144', 1),
(3, '4604', 'CÁLCULO DIFERENCIAL E INTEGRA', '1º Série', 4, '144', '0', '0', '144', 1),
(4, '4606', 'CIRCUITOS DIGITAIS ', '1º Série', 2, '72', '0', '0', '72', 1),
(7, '4609 ', 'ALGEBRA LINEAR E GEOMETRIA ANALITIC', '2º Série ', 2, '72', '0', '0', '72', 1),
(9, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série ', 2, '72', '0', '0', '72', 1),
(10, '4613', 'LINGUAGENS DE PROGRAMAÇÃO', '2º Série ', 4, '144', '0', '0', '144', 1),
(11, '4612', 'MODELAGEM E SIMULAÇÃO', '2º Série ', 2, '72', '0', '0', '72', 1),
(12, '4603', 'SISTEMAS OPERACIONAIS', '2º Série ', 4, '144', '0', '0', '144', 1),
(13, '4162', 'SUPORTE DE HARDWARE', '2º Série ', 2, '72', '0', '0', '72', 1),
(14, '4619', 'BANCO DE DADOS', '3º Série', 4, '144', '0', '0', '144', 1),
(15, '4615', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 1),
(16, '4621', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '3º Série', 2, '72', '0', '0', '72', 1),
(17, '4629', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '3º Série', 2, '72', '0', '0', '72', 1),
(18, '4618', 'PRÁTICA E GERENCIAMENTO DE PROJETOS ', '3º Série', 2, '72', '0', '0', '72', 1),
(19, '4617', 'PROBABILIDADE E ESTATÍSTICA E PLANEJAMENTO DE', '3º Série', 2, '72', '0', '0', '72', 1),
(20, '4610', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDO', '3º Série', 4, '144', '0', '0', '144', 1),
(21, '4608', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 1),
(22, '425', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 1),
(23, '4623', 'AUDITORIA E AVALIAÇÃO DE SISTEMAS', '4º Série', 2, '72', '0', '0', '72', 1),
(24, '4628', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 4, '144', '0', '0', '144', 1),
(25, '4614', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 1),
(26, '4626', 'ENGENHARIA DE SOFTWAR', '4º Série', 4, '144', '0', '0', '144', 1),
(27, '97', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '0', '300', '0', '300', 1),
(28, '4625', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 1),
(29, '4627', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 1),
(30, '4630', 'TELECOMUNICAÇÕES E TELEMÁTICA', '4º Série', 2, '72', '0', '0', '72', 1),
(31, '4602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 2),
(32, '4605', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 4, '144', '0', '0', '144', 2),
(33, '4604', 'CÁLCULO DIFERENCIAL E INTEGRA', '1º Série', 4, '144', '0', '0', '144', 2),
(34, '4606', 'CIRCUITOS DIGITAIS', '1º Série', 2, '72', '0', '0', '72', 2),
(35, '4607', 'INGLÊS', '1º Série', 2, '72', '0', '0', '72', 2),
(36, '4611', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRET', '1º Série', 4, '144', '0', '0', '144', 2),
(37, '4609', 'ALGEBRA LINEAR E GEOMETRIA ANALITICA', '2º Série', 2, '72', '0', '0', '72', 2),
(38, '4601', 'ESTRUTURA DE DADOS, PESQUISA E ORDENAÇÃO', '2º Série', 4, '144', '0', '0', '144', 2),
(39, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '72', '0', '0', '72', 2),
(40, '4613', 'LINGUAGENS DE PROGRAMAÇÃO', '2º Série', 4, '144', '0', '0', '144', 2),
(41, '4612', 'MODELAGEM E SIMULAÇÃO', '2º Série', 2, '72', '0', '0', '72', 2),
(42, '4603', 'SISTEMAS OPERACIONAIS', '2º Série', 4, '144', '0', '0', '144', 2),
(43, '4162', 'SUPORTE DE HARDWARE', '2º Série', 2, '72', '0', '0', '72', 2),
(44, '4619', 'BANCO DE DADOS', '3º Série', 4, '144', '0', '0', '144', 2),
(45, '4615', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 2),
(46, '4621', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '3º Série', 2, '72', '0', '0', '72', 2),
(47, '4629', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '3º Série', 2, '72', '0', '0', '72', 2),
(48, '4618', 'PRÁTICA E GERENCIAMENTO DE PROJETO', '3º Série', 2, '72', '0', '0', '72', 2),
(49, '4617', 'PROBABILIDADE E ESTATÍSTICA E PLANEJAMENTO DE', '3º Série', 2, '72', '0', '0', '72', 2),
(50, '4610', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '144', '0', '0', '144', 2),
(51, '4608', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 2),
(52, '425', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 2),
(53, '4623', 'AUDITORIA E AVALIAÇÃO DE SISTEMAS', '4º Série', 2, '72', '0', '0', '72', 2),
(54, '4628', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 4, '144', '0', '0', '144', 2),
(55, '4614', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 2),
(56, '4626', 'ENGENHARIA DE SOFTWARE', '4º Série', 4, '144', '0', '0', '144', 2),
(57, '97', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '220', '0', '0', '220', 2),
(58, '4625', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 2),
(59, '4627', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 2),
(60, '4630', 'TELECOMUNICAÇÕES E TELEMÁTICA', '4º Série', 2, '72', '0', '0', '72', 2),
(61, '4602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 3),
(62, '4605', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 2, '72', '0', '0', '72', 3),
(63, '4604', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '144', '0', '0', '144', 3),
(64, '4606', 'CIRCUITOS DIGITAIS', '1º Série', 2, '72', '0', '0', '72', 3),
(65, '279', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '72', '0', '0', '72', 3),
(66, '4334', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '1º Série', 2, '72', '0', '0', '72', 3),
(67, '4611', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '72', '0', '0', '72', 3),
(68, '995', 'OPTATIVA I', '1º Série', 2, '72', '0', '0', '72', 3),
(69, '4609', 'ALGEBRA LINEAR E GEOMETRIA ANALITICA', '2º Série', 2, '72', '0', '0', '72', 3),
(70, '4636', 'BANCO DE DADOS I', '2º Série', 2, '72', '0', '0', '72', 3),
(71, '4601', 'ESTRUTURA DE DADOS, PESQUISA E ORDENAÇÃO', '2º Série', 4, '144', '0', '0', '144', 3),
(72, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '72', '0', '0', '72', 3),
(73, '747', 'LINGUAGEM DE PROGRAMAÇÃO I', '2º Série', 4, '144', '0', '0', '144', 3),
(74, '4603', 'SISTEMAS OPERACIONAIS', '2º Série', 2, '72', '0', '0', '72', 3),
(75, '4162', 'SUPORTE DE HARDWARE', '2º Série', 2, '72', '0', '0', '72', 3),
(76, '996', 'OPTATIVA II', '2º Série', 2, '72', '0', '0', '72', 3),
(77, '4632', 'BANCO DE DADOS II', '3º Série', 2, '72', '0', '0', '72', 3),
(78, '4615', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 3),
(79, '834', 'LINGUAGEM DE PROGRAMAÇÃO II', '3º Série', 4, '144', '0', '0', '144', 3),
(80, '4618', 'PRÁTICA E GERENCIAMENTO DE PROJETOS', '3º Série', 2, '72', '0', '0', '72', 3),
(81, '3215', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '72', '0', '0', '72', 3),
(82, '4610', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 2, '72', '0', '0', '72', 3),
(83, '4633', 'SEGURANÇA, AUDITORIA E AVALIÇÃO DE SISTEMAS', '3º Série', 2, '72', '0', '0', '72', 3),
(84, '4608', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 3),
(85, '997', 'OPTATIVA II', '3º Série', 2, '72', '0', '0', '72', 3),
(86, '4621', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 2, '72', '0', '0', '72', 3),
(87, '4628', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 2, '72', '0', '0', '72', 3),
(88, '4614', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 3),
(89, '4626', 'ENGENHARIA DE SOFTWARE', '4º Série', 4, '144', '0', '0', '144', 3),
(90, '4625', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 3),
(91, '4627', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 3),
(92, '4616', 'TELECOMUNICAÇÕES', '4º Série', 2, '72', '0', '0', '72', 3),
(93, '998', 'OPTATIVA IV', '4º Série', 2, '72', '0', '0', '72', 3),
(94, '425', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 3),
(95, '97', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '220', '0', '0', '220', 3),
(96, '4638', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIA', 'Optativa', 2, '72', '0', '0', '72', 3),
(97, '4643', 'COMPORTAMENTO ORGANIZACIONAL', 'Optativa', 2, '72', '0', '0', '72', 3),
(98, '4637', 'DIREITO E LEGISLAÇÃO', 'Optativa', 2, '72', '0', '0', '72', 3),
(99, '4640', 'INFORMÁTICA NA EDUCAÇÃO', 'Optativa', 2, '72', '0', '0', '72', 3),
(100, '4645', 'LÍNGUA BRASILEIRA DE SINAIS - LIBRAS', 'Optativa', 2, '72', '0', '0', '72', 3),
(101, '4646', 'LÍNGUA PORTUGUESA', 'Optativa', 2, '72', '0', '0', '72', 3),
(102, '4641', 'MÉTODOS QUANTITATIVOS', 'Optativa', 2, '72', '0', '0', '72', 3),
(103, '4642', 'MÉTODOS QUANTITATIVOS APLICADOS A ADMINISTRAÇ', 'Optativa', 2, '72', '0', '0', '72', 3),
(104, '4644', 'PROJETO E ANÁLISE DE SISTEMA', 'Optativa', 2, '72', '0', '0', '72', 3),
(105, '4639', 'TEORIA GERAL DO SISTEMA', 'Optativa', 2, '72', '0', '0', '72', 3),
(106, '4635', 'TÓPICOS DE MATEMÁTICA', 'Optativa', 2, '72', '0', '0', '72', 3),
(107, '4602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 4),
(108, '4604', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 4),
(109, '279', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 4),
(110, '7123', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 4),
(111, '4611', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 4),
(112, '268', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 4),
(113, '4606', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 4),
(114, '1494', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 4),
(115, '1492', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 4),
(116, '416', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 4),
(117, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 4),
(118, '1101', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 4),
(119, '1493', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 4),
(120, '1532', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 4),
(121, '1496', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 4),
(122, '801', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 4),
(123, '205', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 4),
(124, '996', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 4),
(125, '3215', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 4),
(126, '1191', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 4),
(127, '1815', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 4),
(128, '391', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 4),
(129, '4614', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 4),
(130, '1481', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 4),
(131, '948', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 4),
(132, '997', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 4),
(133, '2653', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 4),
(134, '1490', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 4),
(135, '1490', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 4),
(136, '4638', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 4),
(137, '2813', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 4),
(138, '1250', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 4),
(139, '2227', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 4),
(140, '2851', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 4),
(141, '1388', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 4),
(142, '2158', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 4),
(147, '', 'DISCIPLINA TESTE', '1º Série', 20, '', '', '', '0', 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `professor`
--

CREATE TABLE IF NOT EXISTS `professor` (
  `professor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`professor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao_matriz_disciplina`
--

CREATE TABLE IF NOT EXISTS `situacao_matriz_disciplina` (
  `situacao_matriz_disciplina_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL DEFAULT '',
  `disciplina` varchar(45) NOT NULL DEFAULT '',
  `serie` varchar(45) NOT NULL DEFAULT '',
  `credito` int(10) unsigned NOT NULL DEFAULT '0',
  `ch` varchar(50) NOT NULL DEFAULT '',
  `chp` varchar(45) NOT NULL DEFAULT '',
  `cho` varchar(45) NOT NULL DEFAULT '',
  `chtotal` varchar(45) NOT NULL DEFAULT '',
  `aluno_id` int(10) unsigned NOT NULL DEFAULT '0',
  `situacao` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`situacao_matriz_disciplina_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=223 ;

--
-- Extraindo dados da tabela `situacao_matriz_disciplina`
--

INSERT INTO `situacao_matriz_disciplina` (`situacao_matriz_disciplina_id`, `codigo`, `disciplina`, `serie`, `credito`, `ch`, `chp`, `cho`, `chtotal`, `aluno_id`, `situacao`) VALUES
(194, '', 'ALGEBRA LINEAR E GEOMETRIA ANALITIC', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(195, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 1, 2),
(196, '', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 4, '144', '0', '0', '144', 1, 3),
(197, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 1, 0),
(198, '', 'AUDITORIA E AVALIAÇÃO DE SISTEMAS', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(199, '', 'BANCO DE DADOS', '3º Série', 4, '144', '0', '0', '144', 1, 0),
(200, '', 'CÁLCULO DIFERENCIAL E INTEGRA', '1º Série', 4, '144', '0', '0', '144', 1, 2),
(201, '', 'CIRCUITOS DIGITAIS ', '1º Série', 2, '72', '0', '0', '72', 1, 1),
(202, '', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(203, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(204, '', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 4, '144', '0', '0', '144', 1, 0),
(205, '', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(206, '', 'ENGENHARIA DE SOFTWAR', '4º Série', 4, '144', '0', '0', '144', 1, 0),
(207, '', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '0', '300', '0', '300', 1, 0),
(208, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(209, '', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 1, 0),
(210, '', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(211, '', 'LINGUAGENS DE PROGRAMAÇÃO', '2º Série ', 4, '144', '0', '0', '144', 1, 1),
(212, '', 'MODELAGEM E SIMULAÇÃO', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(213, '', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(214, '', 'PRÁTICA E GERENCIAMENTO DE PROJETOS ', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(215, '', 'PROBABILIDADE E ESTATÍSTICA E PLANEJAMENTO DE', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(216, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDO', '3º Série', 4, '144', '0', '0', '144', 1, 0),
(217, '', 'SISTEMAS OPERACIONAIS', '2º Série ', 4, '144', '0', '0', '144', 1, 0),
(218, '', 'SUPORTE DE HARDWARE', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(219, '', 'TELECOMUNICAÇÕES E TELEMÁTICA', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(220, '', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL DEFAULT '',
  `senha` varchar(45) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `login` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`usuario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario`, `senha`, `status`, `login`) VALUES
(3, 'Administrador', 'admin', 1, 'admin'),
(5, 'Igreja Agape', 'agape', 1, 'agape');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
