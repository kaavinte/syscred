
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 29/10/2017 às 21:32:37
-- Versão do Servidor: 10.1.24-MariaDB
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u181088591_sys`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE IF NOT EXISTS `aluno` (
  `aluno_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `matricula` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `senha` varchar(45) NOT NULL DEFAULT '',
  `status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0-INATIVO 1-ATIVO',
  `cpf` varchar(45) NOT NULL DEFAULT '',
  `rg` varchar(45) NOT NULL DEFAULT '',
  `orgao` varchar(45) NOT NULL DEFAULT '',
  `genero` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '1-MASCULINO 2- FEMININO',
  `naturalidade` varchar(45) NOT NULL DEFAULT '',
  `nacionalidade` varchar(45) NOT NULL DEFAULT '',
  `endereco` varchar(200) NOT NULL DEFAULT '',
  `cidade` varchar(45) NOT NULL DEFAULT '',
  `uf` varchar(45) NOT NULL DEFAULT '',
  `cep` varchar(45) NOT NULL DEFAULT '',
  `matriz_id` int(10) unsigned NOT NULL DEFAULT '0',
  `curso_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aluno_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`aluno_id`, `nome`, `matricula`, `email`, `senha`, `status`, `cpf`, `rg`, `orgao`, `genero`, `naturalidade`, `nacionalidade`, `endereco`, `cidade`, `uf`, `cep`, `matriz_id`, `curso_id`) VALUES
(1, 'KAROLINE OLIVEIRA', 306020, 'karol_ingrid_avinte@hotmail.com', '123456', 1, '', '24101290', '', 2, '', '', '', '', '', '', 1, 0),
(50, 'Vanderson Souza', 1010102, 'vs93@hotmail.com', 'vandi', 1, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(51, 'Danryel Carvalho ', 811, 'danielekrystal@gmail.com@gmail.com', 'dan', 1, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(52, 'janilson', 148461540, 'janilson.sampaio@gmail.com', '102738', 1, '', '', '', 0, '', '', '', '', '', '', 3, 3),
(54, 'nilson', 148461540, 'janilson.sampaio@gmail.com', '10273839', 1, '', '', '', 0, '', '', '', '', '', '', 4, 3),
(53, 'janilson sampaio', 148461540, 'janilson.sampaio@gmail.com', '102738', 0, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(31, 'Brunno Soares', 148461426, 'brunnosoars@gmail.com', '14012010helenbb', 1, '', '', '', 1, '', '', '', '', '', '', 4, 3),
(36, 'Everton Cunha ', 148461423, 'evertonsdacunha@gmail.com', '123', 1, '', '', '', 0, '', '', '', '', '', '', 4, 3),
(49, 'Pedro Chaves', 148461440, 'tito_azevedo19@hotmail.com', '123456789', 1, '', '', '', 0, '', '', '', '', '', '', 4, 3),
(48, 'Pedro', 148461223, 'pedroazevedo965@gmail.com', '94902899', 1, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(47, 'Janilaon', 148461540, 'Janilson.sampaio@gmail.com', '10273839', 0, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(44, '', 0, '', '', 0, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(45, 's', 0, 's', 's', 1, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(38, 'MURY', 148461404, 'muryelcarvalho@hotmail.com', 'nina', 1, '01646300238', '18760635', 'SSP', 1, 'Manaus', '', 'Rua 43 Quadra 153', 'Manaus', 'AM', '69099210', 4, 3),
(43, '', 0, '', '', 0, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(46, 'Raphael Fontinelle Zany', 148461430, 'zanyfontinelle@gmail.com', 'fontinellerafa', 1, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(39, 'Rebeca Vieira Batista', 1234, 'rebeeca.vieeira14@hotmail.com', '087420', 1, '02673802240', '28509587', 'Ssp', 2, '', '', '', '', '', '69097746', 0, 3),
(40, 'Daniel Ribeiro da Silva ', 0, 'Danielekrystal@gmail.com ', 'danryel0811', 1, '', '', '', 0, '', '', '', '', '', '', 0, 3),
(41, 'saulo augusto de oliveira Monteiro', 148461400, 'saulo.monteiro8@gmail.com', '270492sm7', 1, '00854779230', '23897880', 'sesp', 1, 'amazonense', '', 'cel ferreira de araujo, n° 208', 'manaus', 'amazonas', '69063000', 1, 3),
(42, 'Luiz Eduardo da Silva e Silva ', 148461366, 'luizjudo@hotmail.com ', '@dminti35', 1, '52794334204', '17846269', 'Ssp', 1, 'Manaus', '', 'Rua pasteur', 'Manaus', 'Am', '69043750', 4, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `credito`
--

CREATE TABLE IF NOT EXISTS `credito` (
  `credito_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_atividade` varchar(200) NOT NULL DEFAULT '',
  `data_inicio` date NOT NULL DEFAULT '0000-00-00',
  `data_termino` date NOT NULL DEFAULT '0000-00-00',
  `carga_horaria` varchar(45) NOT NULL DEFAULT '',
  `local` longtext NOT NULL,
  `aluno_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`credito_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Extraindo dados da tabela `credito`
--

INSERT INTO `credito` (`credito_id`, `nome_atividade`, `data_inicio`, `data_termino`, `carga_horaria`, `local`, `aluno_id`) VALUES
(1, 'ATIVIDADE TESTE', '2016-11-01', '2016-11-27', '20', 'AUDITÓRIO CIESA', 1),
(2, 'reds', '0000-00-00', '0000-00-00', '150', 'uea', 32),
(3, 'redes', '2015-10-19', '2015-10-20', '100', 'Ciesa', 35),
(4, 'Java Jdk', '2016-11-10', '2016-11-12', '60', 'La salle', 35),
(5, 'EGRESSO ', '2016-06-01', '2016-06-02', '100', 'CIESA', 33),
(6, 'EGRESSO ', '2016-12-12', '2016-12-12', '100', 'CIESA', 38),
(7, 'rede', '0000-00-00', '0000-00-00', '150', 'UEA', 37),
(8, 'teste', '0000-00-00', '0000-00-00', '20', 'Ciesa', 31),
(9, 'INFORWEEK', '2016-12-01', '2016-12-01', '52', 'CIESA', 38),
(13, '2 encontro de software e egressos', '2016-06-27', '2016-06-27', '50', 'Ciesa', 52),
(11, 'teste 2 ', '0000-00-00', '0000-00-00', '50', 'cd 22', 31),
(12, 'CURSO ANDROID', '2016-12-06', '2016-12-07', '100', 'CIESA', 38),
(18, 'Semana Linux', '2016-10-26', '2016-10-28', '50', 'Ciesa', 54),
(16, 'Palestra de Pedagogia', '2016-10-20', '2016-10-20', '20', 'Ciesa', 38),
(17, 'SEMANA DE ECONOMIA ', '2016-05-01', '2016-05-03', '136', 'CIESA', 38);

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
  `curso_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL DEFAULT '',
  `abreviatura` varchar(45) NOT NULL DEFAULT '',
  `coordenador` varchar(45) NOT NULL DEFAULT '',
  `duracao` varchar(45) NOT NULL DEFAULT '',
  `ativ_comp_obrigatoria` varchar(45) NOT NULL DEFAULT '',
  `estagio_obrigatoria` varchar(45) NOT NULL DEFAULT '',
  `valor` double NOT NULL DEFAULT '0',
  `habilitacao` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`curso_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`curso_id`, `descricao`, `abreviatura`, `coordenador`, `duracao`, `ativ_comp_obrigatoria`, `estagio_obrigatoria`, `valor`, `habilitacao`) VALUES
(3, 'CIENCIA DA COMPUTACAO', 'CCP', 'JANNAINY SENA', '8', '408', '200', 450, 'CCP');

-- --------------------------------------------------------

--
-- Estrutura da tabela `instituicao`
--

CREATE TABLE IF NOT EXISTS `instituicao` (
  `instituicao_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL DEFAULT '',
  `ch` varchar(45) NOT NULL DEFAULT '',
  `media` varchar(45) NOT NULL DEFAULT '',
  `endereco` varchar(45) NOT NULL DEFAULT '',
  `obs` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`instituicao_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `instituicao`
--

INSERT INTO `instituicao` (`instituicao_id`, `nome`, `ch`, `media`, `endereco`, `obs`) VALUES
(1, 'CIESA', '500', '8', 'AV DJALMA BASTISTA', 'TESTE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `matriz`
--

CREATE TABLE IF NOT EXISTS `matriz` (
  `matriz_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL DEFAULT '',
  `ano` varchar(45) NOT NULL DEFAULT '',
  `curso_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`matriz_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `matriz`
--

INSERT INTO `matriz` (`matriz_id`, `nome`, `ano`, `curso_id`) VALUES
(1, 'MATRIZ 146 CCP', '2011', 3),
(2, 'MATRIZ 147 CCP', '2012', 3),
(3, 'MATRIZ  148 CCP', '2013', 3),
(4, 'MATRIZ 149 CCP', '2014', 3),
(7, 'MATRIZ 050 SIE', '2016', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `matriz_disciplina`
--

CREATE TABLE IF NOT EXISTS `matriz_disciplina` (
  `matriz_disciplina_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL DEFAULT '',
  `disciplina` varchar(45) NOT NULL DEFAULT '',
  `serie` varchar(45) NOT NULL DEFAULT '',
  `credito` int(10) unsigned NOT NULL DEFAULT '0',
  `ch` varchar(50) NOT NULL DEFAULT '',
  `chp` varchar(45) NOT NULL DEFAULT '',
  `cho` varchar(45) NOT NULL DEFAULT '',
  `chtotal` varchar(45) NOT NULL DEFAULT '',
  `matriz_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`matriz_disciplina_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Extraindo dados da tabela `matriz_disciplina`
--

INSERT INTO `matriz_disciplina` (`matriz_disciplina_id`, `codigo`, `disciplina`, `serie`, `credito`, `ch`, `chp`, `cho`, `chtotal`, `matriz_id`) VALUES
(1, '40602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 1),
(2, '4605', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 4, '144', '0', '0', '144', 1),
(3, '4604', 'CÁLCULO DIFERENCIAL E INTEGRA', '1º Série', 4, '144', '0', '0', '144', 1),
(4, '4606', 'CIRCUITOS DIGITAIS ', '1º Série', 2, '72', '0', '0', '72', 1),
(7, '4609 ', 'ALGEBRA LINEAR E GEOMETRIA ANALITIC', '2º Série ', 2, '72', '0', '0', '72', 1),
(9, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série ', 2, '72', '0', '0', '72', 1),
(10, '4613', 'LINGUAGENS DE PROGRAMAÇÃO', '2º Série ', 4, '144', '0', '0', '144', 1),
(11, '4612', 'MODELAGEM E SIMULAÇÃO', '2º Série ', 2, '72', '0', '0', '72', 1),
(12, '4603', 'SISTEMAS OPERACIONAIS', '2º Série ', 4, '144', '0', '0', '144', 1),
(13, '4162', 'SUPORTE DE HARDWARE', '2º Série ', 2, '72', '0', '0', '72', 1),
(14, '4619', 'BANCO DE DADOS', '3º Série', 4, '144', '0', '0', '144', 1),
(15, '4615', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 1),
(16, '4621', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '3º Série', 2, '72', '0', '0', '72', 1),
(17, '4629', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '3º Série', 2, '72', '0', '0', '72', 1),
(18, '4618', 'PRÁTICA E GERENCIAMENTO DE PROJETOS ', '3º Série', 2, '72', '0', '0', '72', 1),
(19, '4617', 'PROBABILIDADE E ESTATÍSTICA E PLANEJAMENTO DE', '3º Série', 2, '72', '0', '0', '72', 1),
(20, '4610', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDO', '3º Série', 4, '144', '0', '0', '144', 1),
(21, '4608', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 1),
(22, '425', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 1),
(23, '4623', 'AUDITORIA E AVALIAÇÃO DE SISTEMAS', '4º Série', 2, '72', '0', '0', '72', 1),
(24, '4628', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 4, '144', '0', '0', '144', 1),
(25, '4614', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 1),
(26, '4626', 'ENGENHARIA DE SOFTWAR', '4º Série', 4, '144', '0', '0', '144', 1),
(27, '97', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '0', '300', '0', '300', 1),
(28, '4625', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 1),
(29, '4627', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 1),
(30, '4630', 'TELECOMUNICAÇÕES E TELEMÁTICA', '4º Série', 2, '72', '0', '0', '72', 1),
(31, '4602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 2),
(32, '4605', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 4, '144', '0', '0', '144', 2),
(33, '4604', 'CÁLCULO DIFERENCIAL E INTEGRA', '1º Série', 4, '144', '0', '0', '144', 2),
(34, '4606', 'CIRCUITOS DIGITAIS', '1º Série', 2, '72', '0', '0', '72', 2),
(35, '4607', 'INGLÊS', '1º Série', 2, '72', '0', '0', '72', 2),
(36, '4611', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRET', '1º Série', 4, '144', '0', '0', '144', 2),
(37, '4609', 'ALGEBRA LINEAR E GEOMETRIA ANALITICA', '2º Série', 2, '72', '0', '0', '72', 2),
(38, '4601', 'ESTRUTURA DE DADOS, PESQUISA E ORDENAÇÃO', '2º Série', 4, '144', '0', '0', '144', 2),
(39, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '72', '0', '0', '72', 2),
(40, '4613', 'LINGUAGENS DE PROGRAMAÇÃO', '2º Série', 4, '144', '0', '0', '144', 2),
(41, '4612', 'MODELAGEM E SIMULAÇÃO', '2º Série', 2, '72', '0', '0', '72', 2),
(42, '4603', 'SISTEMAS OPERACIONAIS', '2º Série', 4, '144', '0', '0', '144', 2),
(43, '4162', 'SUPORTE DE HARDWARE', '2º Série', 2, '72', '0', '0', '72', 2),
(44, '4619', 'BANCO DE DADOS', '3º Série', 4, '144', '0', '0', '144', 2),
(45, '4615', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 2),
(46, '4621', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '3º Série', 2, '72', '0', '0', '72', 2),
(47, '4629', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '3º Série', 2, '72', '0', '0', '72', 2),
(48, '4618', 'PRÁTICA E GERENCIAMENTO DE PROJETO', '3º Série', 2, '72', '0', '0', '72', 2),
(49, '4617', 'PROBABILIDADE E ESTATÍSTICA E PLANEJAMENTO DE', '3º Série', 2, '72', '0', '0', '72', 2),
(50, '4610', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '144', '0', '0', '144', 2),
(51, '4608', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 2),
(52, '425', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 2),
(53, '4623', 'AUDITORIA E AVALIAÇÃO DE SISTEMAS', '4º Série', 2, '72', '0', '0', '72', 2),
(54, '4628', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 4, '144', '0', '0', '144', 2),
(55, '4614', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 2),
(56, '4626', 'ENGENHARIA DE SOFTWARE', '4º Série', 4, '144', '0', '0', '144', 2),
(57, '97', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '220', '0', '0', '220', 2),
(58, '4625', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 2),
(59, '4627', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 2),
(60, '4630', 'TELECOMUNICAÇÕES E TELEMÁTICA', '4º Série', 2, '72', '0', '0', '72', 2),
(61, '4602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 3),
(62, '4605', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 2, '72', '0', '0', '72', 3),
(63, '4604', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '144', '0', '0', '144', 3),
(64, '4606', 'CIRCUITOS DIGITAIS', '1º Série', 2, '72', '0', '0', '72', 3),
(65, '279', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '72', '0', '0', '72', 3),
(66, '4334', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '1º Série', 2, '72', '0', '0', '72', 3),
(67, '4611', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '72', '0', '0', '72', 3),
(68, '995', 'OPTATIVA I', '1º Série', 2, '72', '0', '0', '72', 3),
(69, '4609', 'ALGEBRA LINEAR E GEOMETRIA ANALITICA', '2º Série', 2, '72', '0', '0', '72', 3),
(70, '4636', 'BANCO DE DADOS I', '2º Série', 2, '72', '0', '0', '72', 3),
(71, '4601', 'ESTRUTURA DE DADOS, PESQUISA E ORDENAÇÃO', '2º Série', 4, '144', '0', '0', '144', 3),
(72, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '72', '0', '0', '72', 3),
(73, '747', 'LINGUAGEM DE PROGRAMAÇÃO I', '2º Série', 4, '144', '0', '0', '144', 3),
(74, '4603', 'SISTEMAS OPERACIONAIS', '2º Série', 2, '72', '0', '0', '72', 3),
(75, '4162', 'SUPORTE DE HARDWARE', '2º Série', 2, '72', '0', '0', '72', 3),
(76, '996', 'OPTATIVA II', '2º Série', 2, '72', '0', '0', '72', 3),
(77, '4632', 'BANCO DE DADOS II', '3º Série', 2, '72', '0', '0', '72', 3),
(78, '4615', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 3),
(79, '834', 'LINGUAGEM DE PROGRAMAÇÃO II', '3º Série', 4, '144', '0', '0', '144', 3),
(80, '4618', 'PRÁTICA E GERENCIAMENTO DE PROJETOS', '3º Série', 2, '72', '0', '0', '72', 3),
(81, '3215', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '72', '0', '0', '72', 3),
(82, '4610', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 2, '72', '0', '0', '72', 3),
(83, '4633', 'SEGURANÇA, AUDITORIA E AVALIÇÃO DE SISTEMAS', '3º Série', 2, '72', '0', '0', '72', 3),
(84, '4608', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 3),
(85, '997', 'OPTATIVA II', '3º Série', 2, '72', '0', '0', '72', 3),
(86, '4621', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 2, '72', '0', '0', '72', 3),
(87, '4628', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 2, '72', '0', '0', '72', 3),
(88, '4614', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 3),
(89, '4626', 'ENGENHARIA DE SOFTWARE', '4º Série', 4, '144', '0', '0', '144', 3),
(90, '4625', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 3),
(91, '4627', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 3),
(92, '4616', 'TELECOMUNICAÇÕES', '4º Série', 2, '72', '0', '0', '72', 3),
(93, '998', 'OPTATIVA IV', '4º Série', 2, '72', '0', '0', '72', 3),
(94, '425', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 3),
(95, '97', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '220', '0', '0', '220', 3),
(96, '4638', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIA', 'Optativa', 2, '72', '0', '0', '72', 3),
(97, '4643', 'COMPORTAMENTO ORGANIZACIONAL', 'Optativa', 2, '72', '0', '0', '72', 3),
(98, '4637', 'DIREITO E LEGISLAÇÃO', 'Optativa', 2, '72', '0', '0', '72', 3),
(99, '4640', 'INFORMÁTICA NA EDUCAÇÃO', 'Optativa', 2, '72', '0', '0', '72', 3),
(100, '4645', 'LÍNGUA BRASILEIRA DE SINAIS - LIBRAS', 'Optativa', 2, '72', '0', '0', '72', 3),
(101, '4646', 'LÍNGUA PORTUGUESA', 'Optativa', 2, '72', '0', '0', '72', 3),
(102, '4641', 'MÉTODOS QUANTITATIVOS', 'Optativa', 2, '72', '0', '0', '72', 3),
(103, '4642', 'MÉTODOS QUANTITATIVOS APLICADOS A ADMINISTRAÇ', 'Optativa', 2, '72', '0', '0', '72', 3),
(104, '4644', 'PROJETO E ANÁLISE DE SISTEMA', 'Optativa', 2, '72', '0', '0', '72', 3),
(105, '4639', 'TEORIA GERAL DO SISTEMA', 'Optativa', 2, '72', '0', '0', '72', 3),
(106, '4635', 'TÓPICOS DE MATEMÁTICA', 'Optativa', 2, '72', '0', '0', '72', 3),
(107, '4602', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 4),
(108, '4604', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 4),
(109, '279', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 4),
(110, '7123', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 4),
(111, '4611', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 4),
(112, '268', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 4),
(113, '4606', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 4),
(114, '1494', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 4),
(115, '1492', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 4),
(116, '416', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 4),
(117, '4620', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 4),
(118, '1101', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 4),
(119, '1493', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 4),
(120, '1532', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 4),
(121, '1496', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 4),
(122, '801', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 4),
(123, '205', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 4),
(124, '996', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 4),
(125, '3215', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 4),
(126, '1191', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 4),
(127, '1815', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 4),
(128, '391', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 4),
(129, '4614', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 4),
(130, '1481', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 4),
(131, '948', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 4),
(132, '997', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 4),
(133, '2653', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 4),
(134, '1490', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 4),
(135, '1490', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 4),
(136, '4638', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 4),
(137, '2813', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 4),
(138, '1250', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 4),
(139, '2227', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 4),
(140, '2851', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 4),
(141, '1388', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 4),
(142, '2158', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 4),
(147, '', 'DISCIPLINA TESTE', '1º Série', 20, '', '', '', '0', 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `professor`
--

CREATE TABLE IF NOT EXISTS `professor` (
  `professor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`professor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao_matriz_disciplina`
--

CREATE TABLE IF NOT EXISTS `situacao_matriz_disciplina` (
  `situacao_matriz_disciplina_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL DEFAULT '',
  `disciplina` varchar(45) NOT NULL DEFAULT '',
  `serie` varchar(45) NOT NULL DEFAULT '',
  `credito` int(10) unsigned NOT NULL DEFAULT '0',
  `ch` varchar(50) NOT NULL DEFAULT '',
  `chp` varchar(45) NOT NULL DEFAULT '',
  `cho` varchar(45) NOT NULL DEFAULT '',
  `chtotal` varchar(45) NOT NULL DEFAULT '',
  `aluno_id` int(10) unsigned NOT NULL DEFAULT '0',
  `situacao` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`situacao_matriz_disciplina_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=611 ;

--
-- Extraindo dados da tabela `situacao_matriz_disciplina`
--

INSERT INTO `situacao_matriz_disciplina` (`situacao_matriz_disciplina_id`, `codigo`, `disciplina`, `serie`, `credito`, `ch`, `chp`, `cho`, `chtotal`, `aluno_id`, `situacao`) VALUES
(194, '', 'ALGEBRA LINEAR E GEOMETRIA ANALITIC', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(195, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 1, 2),
(196, '', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 4, '144', '0', '0', '144', 1, 3),
(197, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 1, 0),
(198, '', 'AUDITORIA E AVALIAÇÃO DE SISTEMAS', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(199, '', 'BANCO DE DADOS', '3º Série', 4, '144', '0', '0', '144', 1, 0),
(200, '', 'CÁLCULO DIFERENCIAL E INTEGRA', '1º Série', 4, '144', '0', '0', '144', 1, 0),
(201, '', 'CIRCUITOS DIGITAIS ', '1º Série', 2, '72', '0', '0', '72', 1, 1),
(202, '', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(203, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(204, '', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 4, '144', '0', '0', '144', 1, 0),
(205, '', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(206, '', 'ENGENHARIA DE SOFTWAR', '4º Série', 4, '144', '0', '0', '144', 1, 0),
(207, '', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '0', '300', '0', '300', 1, 0),
(208, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(209, '', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 1, 0),
(210, '', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(211, '', 'LINGUAGENS DE PROGRAMAÇÃO', '2º Série ', 4, '144', '0', '0', '144', 1, 0),
(212, '', 'MODELAGEM E SIMULAÇÃO', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(213, '', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(214, '', 'PRÁTICA E GERENCIAMENTO DE PROJETOS ', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(215, '', 'PROBABILIDADE E ESTATÍSTICA E PLANEJAMENTO DE', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(216, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDO', '3º Série', 4, '144', '0', '0', '144', 1, 0),
(217, '', 'SISTEMAS OPERACIONAIS', '2º Série ', 4, '144', '0', '0', '144', 1, 0),
(218, '', 'SUPORTE DE HARDWARE', '2º Série ', 2, '72', '0', '0', '72', 1, 0),
(219, '', 'TELECOMUNICAÇÕES E TELEMÁTICA', '4º Série', 2, '72', '0', '0', '72', 1, 0),
(220, '', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 1, 0),
(223, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 31, 1),
(224, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 31, 1),
(225, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 31, 1),
(226, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 31, 1),
(227, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 31, 0),
(228, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 31, 1),
(229, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 31, 1),
(230, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 31, 0),
(231, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 31, 0),
(232, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 31, 0),
(233, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 31, 0),
(234, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 31, 1),
(235, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 31, 1),
(236, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 31, 0),
(237, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 31, 0),
(238, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 31, 0),
(239, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 31, 1),
(240, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 31, 0),
(241, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 31, 0),
(242, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 31, 1),
(243, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 31, 0),
(244, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 31, 1),
(245, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 31, 1),
(246, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 31, 1),
(247, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 31, 1),
(248, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 31, 1),
(249, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 31, 0),
(250, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 31, 1),
(251, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 31, 1),
(252, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 31, 0),
(253, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 31, 0),
(254, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 31, 1),
(255, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 31, 0),
(256, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 31, 1),
(257, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 31, 1),
(258, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 31, 0),
(259, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 32, 1),
(260, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 32, 1),
(261, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 32, 1),
(262, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 32, 1),
(263, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 32, 1),
(264, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 32, 1),
(265, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 32, 1),
(266, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 32, 1),
(267, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 32, 1),
(268, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 32, 1),
(269, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 32, 1),
(270, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 32, 1),
(271, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 32, 1),
(272, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 32, 3),
(273, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 32, 1),
(274, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 32, 1),
(275, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 32, 1),
(276, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 32, 1),
(277, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 32, 1),
(278, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 32, 1),
(279, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 32, 1),
(280, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 32, 1),
(281, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 32, 1),
(282, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 32, 1),
(283, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 32, 1),
(284, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 32, 1),
(285, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 32, 1),
(286, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 32, 1),
(287, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 32, 1),
(288, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 32, 1),
(289, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 32, 1),
(290, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 32, 1),
(291, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 32, 1),
(292, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 32, 1),
(293, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 32, 1),
(294, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 32, 1),
(295, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 33, 1),
(296, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 33, 0),
(297, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 33, 0),
(298, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 33, 0),
(299, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 33, 0),
(300, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 33, 0),
(301, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 33, 0),
(302, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 33, 0),
(303, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 33, 0),
(304, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 33, 0),
(305, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 33, 0),
(306, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 33, 0),
(307, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 33, 0),
(308, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 33, 0),
(309, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 33, 0),
(310, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 33, 0),
(311, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 33, 1),
(312, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 33, 0),
(313, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 33, 0),
(314, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 33, 1),
(315, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 33, 0),
(316, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 33, 0),
(317, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 33, 0),
(318, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 33, 1),
(319, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 33, 0),
(320, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 33, 0),
(321, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 33, 0),
(322, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 33, 0),
(323, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 33, 0),
(324, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 33, 0),
(325, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 33, 0),
(326, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 33, 0),
(327, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 33, 0),
(328, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 33, 1),
(329, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 33, 0),
(330, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 33, 0),
(331, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 35, 3),
(332, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 35, 0),
(333, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 35, 1),
(334, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 35, 3),
(335, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 35, 3),
(336, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 35, 1),
(337, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 35, 2),
(338, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 35, 3),
(339, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 35, 3),
(340, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 35, 1),
(341, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 35, 3),
(342, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 35, 3),
(343, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 35, 3),
(344, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 35, 3),
(345, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 35, 0),
(346, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 35, 0),
(347, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 35, 1),
(348, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 35, 3),
(349, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 35, 0),
(350, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 35, 3),
(351, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 35, 0),
(352, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 35, 1),
(353, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 35, 3),
(354, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 35, 3),
(355, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 35, 3),
(356, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 35, 1),
(357, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 35, 3),
(358, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 35, 3),
(359, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 35, 1),
(360, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 35, 0),
(361, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 35, 3),
(362, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 35, 3),
(363, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 35, 3),
(364, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 35, 3),
(365, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 35, 3),
(366, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 35, 0),
(367, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 38, 1),
(368, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 38, 2),
(369, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 38, 1),
(370, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 38, 1),
(371, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 38, 1),
(372, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 38, 1),
(373, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 38, 1),
(374, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 38, 1),
(375, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 38, 1),
(376, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 38, 1),
(377, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 38, 1),
(378, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 38, 1),
(379, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 38, 1),
(380, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 38, 1),
(381, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 38, 1),
(382, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 38, 1),
(383, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 38, 1),
(384, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 38, 1),
(385, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 38, 1),
(386, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 38, 1),
(387, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 38, 1),
(388, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 38, 1),
(389, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 38, 1),
(390, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 38, 1),
(391, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 38, 1),
(392, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 38, 1),
(393, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 38, 1),
(394, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 38, 1),
(395, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 38, 1),
(396, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 38, 1),
(397, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 38, 1),
(398, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 38, 1),
(399, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 38, 1),
(400, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 38, 1),
(401, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 38, 1),
(402, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 38, 2),
(403, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 37, 1),
(404, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 37, 0),
(405, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 37, 0),
(406, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 37, 0),
(407, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 37, 0),
(408, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 37, 0),
(409, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 37, 0),
(410, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 37, 0),
(411, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 37, 0),
(412, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 37, 0),
(413, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 37, 0),
(414, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 37, 0),
(415, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 37, 0),
(416, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 37, 0),
(417, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 37, 0),
(418, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 37, 0),
(419, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 37, 1),
(420, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 37, 0),
(421, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 37, 0),
(422, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 37, 1),
(423, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 37, 0),
(424, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 37, 0),
(425, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 37, 0),
(426, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 37, 1),
(427, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 37, 0),
(428, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 37, 0),
(429, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 37, 0),
(430, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 37, 0),
(431, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 37, 0),
(432, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 37, 0),
(433, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 37, 0),
(434, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 37, 0),
(435, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 37, 0),
(436, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 37, 0),
(437, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 37, 0),
(438, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 37, 0),
(439, '', 'ALGEBRA LINEAR E GEOMETRIA ANALITIC', '2º Série ', 2, '72', '0', '0', '72', 41, 0),
(440, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '144', '0', '0', '144', 41, 0),
(441, '', 'ARQUITETURA E ORGANIZAÇÃO DE COMPUTADORES', '1º Série', 4, '144', '0', '0', '144', 41, 0),
(442, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '100', '0', '0', '100', 41, 0),
(443, '', 'AUDITORIA E AVALIAÇÃO DE SISTEMAS', '4º Série', 2, '72', '0', '0', '72', 41, 0),
(444, '', 'BANCO DE DADOS', '3º Série', 4, '144', '0', '0', '144', 41, 0),
(445, '', 'CÁLCULO DIFERENCIAL E INTEGRA', '1º Série', 4, '144', '0', '0', '144', 41, 0),
(446, '', 'CIRCUITOS DIGITAIS ', '1º Série', 2, '72', '0', '0', '72', 41, 1),
(447, '', 'COMPILADORES, LINGUAGENS FORMAIS E AUTÔMATOS', '3º Série', 2, '72', '0', '0', '72', 41, 0),
(448, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '3º Série', 2, '72', '0', '0', '72', 41, 0),
(449, '', 'DESENVOLVIMENTO DE SISTEMAS WEB E EMBARCADOS', '4º Série', 4, '144', '0', '0', '144', 41, 0),
(450, '', 'EMPREENDEDORISMO', '4º Série', 2, '72', '0', '0', '72', 41, 0),
(451, '', 'ENGENHARIA DE SOFTWAR', '4º Série', 4, '144', '0', '0', '144', 41, 0),
(452, '', 'ESTÁGIO SUPERVISIONADO', '4º Série', 0, '0', '300', '0', '300', 41, 0),
(453, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série ', 2, '72', '0', '0', '72', 41, 0),
(454, '', 'INTELIGÊNCIA ARTIFICIAL', '4º Série', 4, '144', '0', '0', '144', 41, 0),
(455, '', 'INTRODUÇÃO A METODOLOGIA CIENTÍFICA', '3º Série', 2, '72', '0', '0', '72', 41, 0),
(456, '', 'LINGUAGENS DE PROGRAMAÇÃO', '2º Série ', 4, '144', '0', '0', '144', 41, 0),
(457, '', 'MODELAGEM E SIMULAÇÃO', '2º Série ', 2, '72', '0', '0', '72', 41, 0),
(458, '', 'ORIENTAÇÃO DE ESTÁGIO', '4º Série', 2, '72', '0', '0', '72', 41, 0),
(459, '', 'PRÁTICA E GERENCIAMENTO DE PROJETOS ', '3º Série', 2, '72', '0', '0', '72', 41, 0),
(460, '', 'PROBABILIDADE E ESTATÍSTICA E PLANEJAMENTO DE', '3º Série', 2, '72', '0', '0', '72', 41, 0),
(461, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDO', '3º Série', 4, '144', '0', '0', '144', 41, 0),
(462, '', 'SISTEMAS OPERACIONAIS', '2º Série ', 4, '144', '0', '0', '144', 41, 0),
(463, '', 'SUPORTE DE HARDWARE', '2º Série ', 2, '72', '0', '0', '72', 41, 0),
(464, '', 'TELECOMUNICAÇÕES E TELEMÁTICA', '4º Série', 2, '72', '0', '0', '72', 41, 0),
(465, '', 'TEORIA DOS GRAFOS', '3º Série', 2, '72', '0', '0', '72', 41, 0),
(466, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 42, 0),
(467, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 42, 0),
(468, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 42, 0),
(469, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 42, 0),
(470, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 42, 0),
(471, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 42, 0),
(472, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 42, 0),
(473, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 42, 0),
(474, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 42, 0),
(475, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 42, 0),
(476, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 42, 0),
(477, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 42, 0),
(478, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 42, 0),
(479, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 42, 0),
(480, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 42, 0),
(481, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 42, 0),
(482, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 42, 0),
(483, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 42, 0),
(484, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 42, 0),
(485, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 42, 0),
(486, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 42, 0),
(487, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 42, 0),
(488, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 42, 0),
(489, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 42, 0),
(490, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 42, 0),
(491, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 42, 0),
(492, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 42, 0),
(493, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 42, 0),
(494, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 42, 0),
(495, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 42, 0),
(496, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 42, 0),
(497, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 42, 0),
(498, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 42, 0),
(499, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 42, 0),
(500, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 42, 0),
(501, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 42, 0),
(502, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 36, 1),
(503, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 36, 0),
(504, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 36, 0),
(505, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 36, 2),
(506, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 36, 0),
(507, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 36, 1),
(508, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 36, 1),
(509, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 36, 0),
(510, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 36, 0),
(511, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 36, 0),
(512, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 36, 0),
(513, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 36, 0),
(514, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 36, 0),
(515, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 36, 0),
(516, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 36, 0),
(517, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 36, 0),
(518, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 36, 2),
(519, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 36, 0),
(520, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 36, 0),
(521, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 36, 1),
(522, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 36, 0),
(523, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 36, 0),
(524, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 36, 0),
(525, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 36, 1),
(526, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 36, 0),
(527, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 36, 0),
(528, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 36, 0),
(529, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 36, 0),
(530, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 36, 0),
(531, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 36, 0),
(532, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 36, 0),
(533, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 36, 0),
(534, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 36, 0),
(535, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 36, 3),
(536, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 36, 0),
(537, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 36, 0),
(538, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 49, 1),
(539, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 49, 0),
(540, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 49, 0),
(541, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 49, 0),
(542, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 49, 0),
(543, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 49, 0),
(544, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 49, 0),
(545, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 49, 0),
(546, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 49, 0),
(547, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 49, 0),
(548, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 49, 0),
(549, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 49, 0),
(550, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 49, 0),
(551, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 49, 0),
(552, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 49, 0),
(553, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 49, 0),
(554, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 49, 0),
(555, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 49, 0),
(556, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 49, 0),
(557, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 49, 1),
(558, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 49, 0),
(559, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 49, 0),
(560, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 49, 0),
(561, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 49, 1),
(562, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 49, 0),
(563, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 49, 0),
(564, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 49, 0),
(565, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 49, 0),
(566, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 49, 0),
(567, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 49, 0),
(568, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 49, 0),
(569, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 49, 0),
(570, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 49, 0),
(571, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 49, 0),
(572, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 49, 0),
(573, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 49, 0),
(574, '', 'DISCIPLINA TESTE', '1º Série', 20, '', '', '', '0', 52, 1),
(575, '', ' METODOLOGIA DA PESQUISA CIENTÍFICA ', '1º Série', 2, '80', '0', '0', '80', 54, 1),
(576, '', 'ADMINISTRAÇÃO E RESPONSABILIDADE SOCIAL', 'Optativa', 2, '80', '0', '0', '80', 54, 0),
(577, '', 'ÁLGEBRA LINEAR', '2º Série', 4, '160', '0', '0', '160', 54, 0),
(578, '', 'ALGORÍTIMO E TÉCNICAS DE PROGRAMAÇÃO', '1º Série', 4, '160', '0', '0', '160', 54, 0),
(579, '', 'ATIVIDADES COMPLEMENTARES', '4º Série', 0, '408', '0', '0', '408', 54, 0),
(580, '', 'BANCO DE DADOS', '2º Série', 4, '160', '0', '0', '160', 54, 0),
(581, '', 'CÁLCULO DIFERENCIAL E INTEGRAL', '1º Série', 4, '160', '0', '0', '160', 54, 0),
(582, '', 'COMPUTAÇÃO GRÁFICA E PROCESSAMENTO DE IMAGENS', '4º Série', 4, '160', '0', '0', '160', 54, 0),
(583, '', 'DESENVOLVIMENTO WEB E SOFTWARE EMBARCADO', '4º Série', 4, '160', '0', '0', '160', 54, 0),
(584, '', 'EMPREENDEDORISMO', '4º Série', 4, '160', '0', '0', '160', 54, 0),
(585, '', 'ENGENHARIA DE SOFTWARE ', '3º Série', 4, '160', '0', '0', '160', 54, 0),
(586, '', 'ESTRUTURA DE DADOS, PESQUISA, ORDENAçãO E GRA', '2º Série', 4, '160', '0', '0', '160', 54, 0),
(587, '', 'ÉTICA, COMPUTADORES E SOCIEDADE', '2º Série', 2, '80', '0', '0', '80', 54, 0),
(588, '', 'GERÊNCIA DE PROJETOS', '4º Série', 2, '80', '0', '0', '80', 54, 0),
(589, '', 'INFORMÁTICA NA ADMINISTRAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 54, 0),
(590, '', 'INFORMÁTICA NA EDUCAÇÃO ', 'Optativa', 2, '80', '0', '0', '80', 54, 0),
(591, '', 'INGLÊS INSTRUMENTAL', '1º Série', 2, '80', '0', '0', '80', 54, 0),
(592, '', 'INTELIGÊNCIA ARTIFICIAL E COMPUTACIONAL', '4º Série', 2, '80', '0', '0', '80', 54, 0),
(593, '', 'LABORATÓRIO DE PROJETOS DE SISTEMAS DE INFORM', 'Optativa', 2, '80', '0', '0', '80', 54, 0),
(594, '', 'LEITURA E PRODUÇÃO DE TEXTO', '1º Série', 2, '80', '0', '0', '80', 54, 1),
(595, '', 'LIBRAS ', 'Optativa', 2, '80', '0', '0', '80', 54, 0),
(596, '', 'LINGUAGENS DE PROGRAMAÇÃO', '3º Série', 4, '160', '0', '0', '160', 54, 0),
(597, '', 'LINGUAGENS FORMAIS E AUTOMATOS, COMPILADORES', '3º Série', 4, '160', '0', '0', '160', 54, 0),
(598, '', 'LÓGICA MATEMÁTICA E MATEMÁTICA DISCRETA', '1º Série', 2, '160', '0', '0', '160', 54, 1),
(599, '', 'OPTATIVA I', '2º Série', 2, '80', '0', '0', '80', 54, 0),
(600, '', 'OPTATIVA II', '3º Série', 2, '80', '0', '0', '80', 54, 0),
(601, '', 'OPTATIVA III', '4º Série', 2, '80', '0', '0', '80', 54, 0),
(602, '', 'ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES', '2º Série', 2, '80', '0', '0', '80', 54, 0),
(603, '', 'PROBABILIDADE E ESTATISTICA', '3º Série', 2, '80', '0', '0', '80', 54, 0),
(604, '', 'PROJETO E ANÁLISE DE ALGORÍTMOS', 'Optativa', 2, '80', '0', '0', '80', 54, 0),
(605, '', 'PROJETO FINAL', '4º Série', 2, '80', '0', '0', '80', 54, 0),
(606, '', 'REDES DE COMPUTADORES E SISTEMAS DISTRIBUÍDOS', '3º Série', 4, '160', '0', '0', '160', 54, 0),
(607, '', 'SEGURANÇA E AUDITORIA DE SISTEMA', '4º Série', 2, '80', '0', '0', '80', 54, 0),
(608, '', 'SISTEMAS LÓGICOS E CIRCUITOS DIGITAIS', '1º Série', 2, '80', '0', '0', '80', 54, 0),
(609, '', 'SISTEMAS OPERACIONAIS LOCAIS E DE REDE ', '2º Série', 2, '80', '0', '0', '80', 54, 0),
(610, '', 'TÓPICOS ESPECIAIS EM COMPUTAÇÃO', 'Optativa', 2, '80', '0', '0', '80', 54, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL DEFAULT '',
  `senha` varchar(45) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `login` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario`, `senha`, `status`, `login`) VALUES
(3, 'Administrador', 'admin', 1, 'admin'),
(5, 'Igreja Agape', 'agape', 1, 'agape');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
